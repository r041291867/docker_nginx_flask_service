# How to use:

### Load images from tar files

```
docker load -i file.tar
```

### Build up service

```
docker-compose build
docker-compose up -d
```

### Attach container

```
docker exec -it container_id bash
```
### Leave container

`Ctrl + P` -> `Ctrl + Q`
