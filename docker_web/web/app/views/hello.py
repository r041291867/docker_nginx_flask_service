import flask_restful

from flask_restful import request
from werkzeug.datastructures import FileStorage
from flask import Flask
from flask_restful import Resource, Api, reqparse
from app.extensions import mysql2,restapi,cache
import os
import socket
import time
import logging
from datetime import date,datetime as dt, timedelta
import codecs, hashlib, os, shutil
from app.utils import cache_key

app = Flask(__name__)
api = Api(app=app)

@restapi.resource('/Hello')
#@cache.cached(key_prefix='/Hello')
class HelloWorld(Resource):
#	@cache.cached(timeout=600, key_prefix='/Hello', unless=None)
	def get(self):
#		print("Hello No Cache")
		logging.info('Hello is Logging')
		logging.getLogger('error_Logger').error('Hello is Error')
		logging.getLogger('error_Logger').handlers[0].flush()
		return {'hello': 'world  (HOSTNAME=%s, PID=%s)' % (socket.gethostname(), os.getpid()) }
