#!/usr/bin/python
# -*- coding:utf-8 -*-
import importlib,sys

from flask import Blueprint,current_app, request, make_response, jsonify
from flask_restful import Resource, Api

from . import views_blueprint
from app.extensions import mysql2,restapi,cache
from app.utils import cache_key
from app.common import Common
from flask import request
import textwrap
import gzip
import logging
import json
import decimal
import os
from datetime import date,datetime as dt, timedelta
import base64
import random
import time
import copy
import sys
import io
import numpy as np
# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

@restapi.resource('/prog/fail_detail')
class ProgFailDetail(Resource):
    def get(self, headers=None):

        # 取得request參數
        BU = request.args.get('BU','')
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")        

        result = []
        sql = ''' SELECT a.program_id,a.bu,a.board,a.step,a.open_cases,b.totalsn,round((a.open_cases/a.step),2) as failrate FROM
                  (
                    SELECT a.program_id,a.bu,a.board,step,count(*) as open_cases FROM (select * from ICT_Project.fail_program where before_retest >= 5) a
                    LEFT JOIN ICT_Project.total_step b on a.program_id=b.program_id and a.board=b.board
                    WHERE a.bu='{0}' AND create_time>='{1}' AND create_time <='{2}'
                    GROUP BY a.program_id,a.bu,a.board,step
                  )a
                  left join 
                  (
                    SELECT count(distinct sn) as totalsn,program_id,board from ICT_Project.ict_detail_result GROUP BY program_id,board
                  )b on a.program_id=b.program_id AND a.board=b.board 
                  order by a.open_cases desc'''.format(BU,t1,t2)
        try:
            rows=Common.FetchDB(sql)

            if len(rows)>0:
                for row in rows:
                    result.append({
                        'program_id': row['program_id'],
                        'BU': row['bu'],
                        'PN': row['board'],
                        'total_sn': row['totalsn'],
                        'total': row['step'],
                        'total_fail': float(row['open_cases']),
                        'fail_rate': str(row['failrate']*100)+'%' if row['failrate'] is not None else '0%',
                        'open_cases': float(row['open_cases']),
                    })
 
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/prog/fail_rate')
class ProgFail2(Resource):
    def get(self, headers=None):

        # 取得request參數
        BU = request.args.get('BU', '')
        weeknum = request.args.get('weeknum', '')
        weeknum = weeknum.replace('W', '')
        weekday = dt.now().weekday()
        week_12_ago = date.today() - timedelta(days=weekday + 7 * 12)

        result = {}
        sql = '''SELECT test_type,COUNT(*) as count,BU FROM ICT_Project.fail_program
                 WHERE BU = '{0}' AND WEEKOFYEAR(create_time)='{1}' and before_retest >= 5 GROUP BY test_type '''.format(BU, weeknum)

        try:
            rows = Common.FetchDB(sql)
            # print(sql)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    result[row['test_type']] = row['count']
                # print (result)
            try:
                short = result['short']
            except:
                result['short'] = 0
            try:
                openn = result['open']
            except:
                result['open'] = 0
            try:
                short = result['analog']
            except:
                result['analog'] = 0
            try:
                openn = result['testjet']
            except:
                result['testjet'] = 0
            try:
                short = result['boundary scan']
            except:
                result['boundary scan'] = 0
            try:
                openn = result['analog powered']
            except:
                result['analog powered'] = 0

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result

@restapi.resource('/prog/failstatus_distribution')
class ProgFailDistribution(Resource):
    def get(self, headers=None):
        # 取得request參數
        BU = request.args.get('BU', '')
        program_id = request.args.get('programId', '')
        board = request.args.get('board', '')
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")

        result = []
        opening = {}
        ongoing = {}
        close = {}
        to_be_signed = {}

        sql = '''select convert(count(*), char(6)) as count, test_type, bu 
                from ICT_Project.fail_program 
                where BU='{0}' and program_id='{1}'  and fail_state = 0 and create_time >='{2}' and create_time <='{3}'
                and before_retest >=5 group by test_type '''.format(BU, program_id, t1, t2)

        sql2 = '''select convert(count(*), char(6)) as count, test_type, bu 
                from ICT_Project.fail_program 
                where BU='{0}' and program_id='{1}'  and fail_state = 1 and create_time >='{2}' and create_time <='{3}'
                group by test_type '''.format(BU, program_id, t1, t2)

        sql3 = '''select convert(count(*), char(6)) as count, test_type, bu 
                from ICT_Project.fail_program 
                where BU='{0}' and program_id='{1}'  and fail_state in (2,3) and create_time >='{2}' and create_time <='{3}'
                group by test_type '''.format(BU, program_id, t1, t2)
        sql4 = '''select convert(count(*), char(6)) as count, test_type, bu 
                        from ICT_Project.fail_program 
                        where BU='{0}' and program_id='{1}'  and fail_state = 4 and create_time >='{2}' and create_time <='{3}'
                        group by test_type '''.format(BU, program_id, t1, t2)
        try:
            rows = Common.FetchDB(sql)
            # print(sql)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    opening[row['test_type']] = str(row['count'])
                # print (result)
            try:
                tmp = opening['analog']
            except:
                opening['analog'] = 0
            try:
                tmp = opening['analog powered']
            except:
                opening['analog powered'] = 0
            try:
                tmp = opening['boundary scan']
            except:
                opening['boundary scan'] = 0
            try:
                tmp = opening['powercheck']
            except:
                opening['powercheck'] = 0
            try:
                tmp = opening['testjet']
            except:
                opening['testjet'] = 0
            try:
                tmp = opening['short']
            except:
                opening['short'] = 0
            try:
                tmp = opening['open']
            except:
                opening['open'] = 0

            rows = Common.FetchDB(sql2)

            # data exist
            if len(rows) > 0:
                for row in rows:
                    ongoing[row['test_type']] = str(row['count'])
                # print (result)
            try:
                tmp = ongoing['analog']
            except:
                ongoing['analog'] = 0
            try:
                tmp = ongoing['analog powered']
            except:
                ongoing['analog powered'] = 0
            try:
                tmp = ongoing['boundary scan']
            except:
                ongoing['boundary scan'] = 0
            try:
                tmp = ongoing['powercheck']
            except:
                ongoing['powercheck'] = 0
            try:
                tmp = ongoing['testjet']
            except:
                ongoing['testjet'] = 0
            try:
                tmp = ongoing['short']
            except:
                ongoing['short'] = 0
            try:
                tmp = ongoing['open']
            except:
                ongoing['open'] = 0

            rows = Common.FetchDB(sql3)
            # print(sql)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    close[row['test_type']] = str(row['count'])
                # print (result)
            try:
                tmp = close['analog']
            except:
                close['analog'] = 0
            try:
                tmp = close['analog powered']
            except:
                close['analog powered'] = 0
            try:
                tmp = close['boundary scan']
            except:
                close['boundary scan'] = 0
            try:
                tmp = close['powercheck']
            except:
                close['powercheck'] = 0
            try:
                tmp = close['testjet']
            except:
                close['testjet'] = 0
            try:
                tmp = close['short']
            except:
                close['short'] = 0
            try:
                tmp = close['open']
            except:
                close['open'] = 0
                
            rows = Common.FetchDB(sql4)
            # print(sql)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    to_be_signed[row['test_type']] = str(row['count'])
                # print (result)
            try:
                tmp = to_be_signed['analog']
            except:
                to_be_signed['analog'] = 0
            try:
                tmp = to_be_signed['analog powered']
            except:
                to_be_signed['analog powered'] = 0
            try:
                tmp = to_be_signed['boundary scan']
            except:
                to_be_signed['boundary scan'] = 0
            try:
                tmp = to_be_signed['powercheck']
            except:
                to_be_signed['powercheck'] = 0
            try:
                tmp = to_be_signed['testjet']
            except:
                to_be_signed['testjet'] = 0
            try:
                tmp = to_be_signed['short']
            except:
                to_be_signed['short'] = 0
            try:
                tmp = to_be_signed['open']
            except:
                to_be_signed['open'] = 0

            result.append(opening)
            result.append(to_be_signed)
            result.append(ongoing)
            result.append(close)

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result


@restapi.resource('/prog/fail_rank')
class ProgFailRank(Resource):
    def get(self, headers=None):
        # 取得request參數
        BU = request.args.get('BU','')
        program_id = request.args.get('programId','')
        board = request.args.get('board','')
        t1 = request.args.get('t1','')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")

        result = []
        sql = '''SELECT program_id,BU,item,before_retest AS count
                 FROM ICT_Project.fail_program WHERE BU='{0}' AND program_id='{1}' 
                 AND board = '{2}' AND fail_state = 0 AND create_time >='{3}' AND create_time <='{4}'
                 GROUP BY item ORDER BY count DESC LIMIT 10'''.format(BU,program_id,board,t1,t2)
        # print(sql)
        try:
            rows=Common.FetchDB(sql)
            # print(rows)
            # data exist
            if len(rows)>0:
                for row in rows:
                # print (result)
                    result.append({
                        "component" : row['item'],
                        "count" : row['count']
                        })
 
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/prog/program_detail')
class ProgFixtureDetail(Resource):
    def get(self, headers=None):
        # 取得request參數
        program_id = request.args.get('programId','')
        board = request.args.get('board','')
        BU = request.args.get('BU','')
        t1 = request.args.get('t1','')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")

        result=''

        opening = {}
        ongoing = {}
        close = {}
        to_be_signed = {}
        
        sql = ''' SELECT a.program_id,a.bu,a.board,a.open_cases,round((a.open_cases/a.step),2) as failrate FROM
                  (
                    SELECT a.program_id,a.bu,a.board,step,count(*) as open_cases FROM ICT_Project.fail_program a
                    LEFT JOIN ICT_Project.total_step b on a.program_id=b.program_id and a.board=b.board
                    WHERE a.bu='{0}' AND a.program_id='{1}' AND a.board='{2}' AND create_time >='{3}' AND create_time <='{4}'
                    GROUP BY a.program_id,a.bu,a.board,step
                  )a '''.format(BU,program_id,board,t1,t2)

        # 計算治具狀態(Opening,Ongoing,close)
        sql2 = '''SELECT fail_state,count(*) as count FROM ICT_Project.fail_program 
                  WHERE bu='{0}' AND program_id='{1}' AND board='{2}' AND create_time>='{3}' AND create_time <='{4}' and before_retest >= 5
                  AND fail_state in (0,1,2,4) GROUP BY fail_state '''.format(BU,program_id,board,t1,t2)

        try:
            rows=Common.FetchDB(sql)
            count=Common.FetchDB(sql2)

            # data exist
            if (rows):
                for row in rows:
                    result=({
                        'program_id': row['program_id'],
                        'BU': row['bu'],
                        'board': row['board'],
                        'failcount': row['open_cases'],
                        'failrate':str(round(row['failrate']*100,2))+'%' if row['failrate'] !=None else '0%',
                        'opening' : 0,
                        'to_be_signed':0,
                        'ongoing' : 0,
                        'close' : 0
                    })
                    if (count):
                        for cnt in count:
                            if (cnt['fail_state']==0):
                                result['opening'] = cnt['count']
                            elif (cnt['fail_state']==4):
                                result['to_be_signed'] = cnt['count']
                            elif (cnt['fail_state']==1):
                                result['ongoing'] = cnt['count']
                            elif (cnt['fail_state']==2):
                                result['close'] = cnt['count']

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR2": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result


@restapi.resource('/prog/failstep_detail')
class ProgFailStepDetail(Resource):
    def get(self, headers=None):
        # 取得request參數
        program_id = request.args.get('programId','')
        board = request.args.get('board','')
        BU = request.args.get('BU','')

        try:

            result = []
            sn_count = 0
            sql = '''SELECT count(distinct sn) as count FROM ICT_Project.ict_detail_result WHERE program_id='{0}' 
                     AND board='{1}' '''.format(program_id,board)

            sn_count=Common.FetchDB(sql)[0]['count']

            sql = '''SELECT item,cpk,round(before_retest/{0},4) as yield FROM ICT_Project.fail_program WHERE fail_state=0
                     AND program_id='{1}' AND board='{2}' AND bu='{3}' AND test_type not in ('open','short') '''.format(sn_count,program_id,board,BU)
            rows = Common.FetchDB(sql)

            if (rows):
                for row in rows:
                    result.append({
                        'components': row['item'],
                        'cpk': str(round(row['cpk'],2)) if row['cpk'] else 'None',
                        'yield': str(round((row['yield'])*100,2))+'%' if not row['cpk'] else 'None'
                    })
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR2": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/prog/fail_trend')
class ProgFailTrend(Resource):
    def get(self, headers=None):

        # 取得request參數
        BU = request.args.get('BU', '')
        # 12周前周一的日期
        weekday = dt.now().weekday()
        week_12_ago = date.today() - timedelta(days=weekday + 7 * 12)

        result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        try:
            weeklist = Common.WeekNumList(13)

            sql = ''' SELECT COUNT(*) as count,WEEKOFYEAR(create_time) as week  FROM ICT_Project.fail_program 
                      WHERE BU = '{0}' AND create_time >= '{1}' and before_retest >= 5 
                      group by WEEKOFYEAR(create_time)'''.format(BU, week_12_ago)
            rows = Common.FetchDB(sql)

            for week in weeklist:
                for row in rows:
                    if (row['week'] == week):
                        result[weeklist.index(week)] = row['count']

            result = result[::-1]
            weeklist = ['W' + str(i) for i in weeklist]
            result = [result, weeklist[::-1]]

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result

@restapi.resource('/prog/failstate_detail')
class ProgFailStateDetail(Resource):
    def get(self, headers=None):
        result = {}

        # 取得request參數
        BU = request.args.get('BU','')
        programId = request.args.get('programId','')
        testType = request.args.get('testType','')                
        failState = request.args.get('failState','') 
        listnum = request.args.get('listnum','')
        if listnum == '' : listnum = 0
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2', '')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")
        fail_state=''
        try:
            if failState == '0' :
                fail_state='=0'
            if failState == '1' :
                fail_state='=1'
            if failState == '2' :
                fail_state='in(2,3)'
            if failState == '4' :
                fail_state='=4'

            sql = ''' SELECT uid,item,program_id,board,before_retest,after_retest,before_total_sn,
                      case when {2}=0 then '待提交'
                      when {2}=4 then '待签核' 
                      when {2}=1 then '處理中'
                      when {2}=2 then '已完结' end as state ,
                      after_total_sn,total_times,b.solution as solution1,c.solution as solution2,d.solution as solution3,a.maintain_op,
                      case when {2}=0 then DATE_FORMAT(a.label_time,"%Y-%m-%d %T")
                      else DATE_FORMAT(a.maintain_time,"%Y-%m-%d %T") end as time
                      FROM ICT_Project.fail_program a
                      LEFT join ICT_Project.fail_solution b on a.solution_1=b.id 
                      LEFT join ICT_Project.fail_solution c on a.solution_2=c.id 
                      LEFT join ICT_Project.fail_solution d on a.solution_3=d.id 
                      WHERE BU='{0}' AND program_id='{1}'  AND fail_state {7}
                      AND test_type = '{3}' AND create_time >='{4}' AND create_time <='{5}' AND before_retest >={6}
                      ORDER BY before_retest DESC '''.format(BU,programId,failState,testType, t1, t2, listnum,fail_state)

            result = Common.FetchDB(sql)

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result


@restapi.resource('/prog/failstep_chart')
class ProgFailStepChart(Resource):
    def get(self, headers=None):
        # 取得request參數
        program_id = request.args.get('programId','')
        board = request.args.get('board','')
        BU = request.args.get('BU','')
        item = request.args.get('component','')

        data = []
        result=''
        sql = ''' SELECT count(*) as  count, high_limit,low_limit,nominal,measured from
                  (
                    SELECT high_limit,low_limit,nominal,measured FROM ICT_Project.fail_measured WHERE case_id IN 
                    (
                        SELECT uid FROM ICT_Project.fail_program WHERE BU = '{0}' 
                        AND board = '{1}' AND program_id = '{2}' AND item = '{3}'
                    ) and label_no=2
                  )a group by  high_limit,low_limit,nominal,measured'''.format(BU,board,program_id,item)

        try:
            rows=Common.FetchDB(sql)

            # data exist
            if (rows):
                for row in rows:
                    d=[]
                    d.append(float(row['measured']))
                    d.append(row['count'])
                    data.append(d)

                result={
                    'data':data,
                    'high_limit': float(rows[0]['high_limit']),
                    'low_limit': float(rows[0]['low_limit']), 
                    'nominal': float(rows[0]['nominal']) 
                }
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/prog/failstate_info')
class ProgFailStateInfo(Resource):
    def get(self, headers=None):
        # 取得request參數
        program_id = request.args.get('programId', '')
        board = request.args.get('board', '')
        BU = request.args.get('BU', '')
        item = request.args.get('component', '')
        component = item
        testType = request.args.get('testType', '')
        uid = request.args.get('uid', '')

        frompoint = 'None'
        endpoint = 'None'
        conv_high_limit = ''
        conv_low_limit = ''
        conv_nominal = ''
        result = {}
        sn_count = 0

        sql = "select last_close_time from " \
              "ICT_Project.fail_program where uid = {}".format(uid)
        t1 = Common.FetchDB(sql)
        t1 = t1[0]['last_close_time']
        if not t1:
            t1 = dt.now() + timedelta(weeks=-4)

        sql2 = '''SELECT count(DISTINCT(sn)) as sn_count FROM ICT_Project.ict_detail_result 
                  WHERE board = '{0}' AND program_id = '{1}' 
                  GROUP BY program_id,board'''.format(board, program_id)
        # print(sql2)
        sn_count = Common.FetchDB(sql2)[0]['sn_count']
        if (testType == 'open' or testType == 'short'):
            sql3 = ''' SELECT from_point,end_point from ICT_Project.open_short_fail 
                     WHERE (from_point='{0}' OR end_point='{0}') AND board='{1}' AND fail_type='{2}'
                     ORDER BY end_time desc LIMIT 1 '''.format(component, board, testType)
            point = Common.FetchDB(sql3)
            frompoint = point[0]['from_point']
            endpoint = point[0]['end_point']
        sql_node = "select GROUP_CONCAT(node) as node from ICT_Project.wirelist where board = '{0}' and test_type = '{1}' and component = '{2}' and node not in (SELECT fixed_node from ICT_Project.board_fixed_node where board='{0}') limit 3".format(
            board, testType, item)
        node_list = Common.FetchDB(sql_node)[0]['node']
        result['Node 1'] = 'None'
        result['Node 2'] = 'None'
        result['Node 3'] = 'None'

        try:
            sql = '''SELECT board,test_type as fail_type,item,uid,
                     before_retest+after_retest AS total_component,datacode,lotcode,round(cpk,2) as cpk
                     FROM ICT_Project.fail_program 
                     WHERE uid = {}'''.format(uid)
            rows = Common.FetchDB(sql)
            # data exist
            if (rows):
                row = rows[0]
                uid = row['uid']
                sqllimit = ''' SELECT CASE WHEN test_type='analog' THEN conv_high_limit ELSE round(high_limit,2) END AS high_limit,
                                      CASE WHEN test_type='analog' THEN conv_low_limit ELSE round(low_limit,2) END AS  low_limit,
                                      CASE WHEN test_type='analog' THEN conv_nominal ELSE round(nominal,2) END AS nominal 
                               FROM ICT_Project.fail_measured WHERE case_id={0} AND label_no=2 ORDER BY end_time LIMIT 1'''.format(
                    uid)
                limit = Common.FetchDB(sqllimit)
                if (limit):
                    conv_high_limit = limit[0]['high_limit']
                    conv_low_limit = limit[0]['low_limit']
                    conv_nominal = limit[0]['nominal']
                result['pn'] = row['board']
                result['fail_type'] = row['fail_type']
                result['component'] = row['item']
                result['high_limit'] = conv_high_limit
                result['low_limit'] = conv_low_limit
                result['nominal'] = conv_nominal
                result['total_component_fail'] = row['total_component']
                result['BOM'] = 0
                result['Data code'] = row['datacode'] if row['datacode'] else 'None'
                result['Lot code'] = row['lotcode'] if row['lotcode'] else 'None'
                # result['used_component_count'] = 0
                result['I Bus'] = 'None'
                result['G Bus'] = 'None'
                result['S Bus'] = 'None'
                result['Use Qty(Bot)'] = 'None'
                result['Use Qty(Top)'] = 'None'
                result['cpk'] = str(row['cpk'])
                result['yield'] = str(round(row['total_component'] / sn_count, 2) * 100) + '%' if not row[
                    'cpk'] else 'None'
                if node_list:
                    for i, node in enumerate(node_list.split(',')):
                        result['Node ' + str(i + 1)] = node

                for i in ['1', '2', '3']:
                    sql = "SELECT GROUP_CONCAT(component) as component,count(*) from ICT_Project.wirelist where node ='{0}' and board = '{1}' and component != '{2}' and test_type = '{3}'".format(
                        result['Node ' + i], board, item, testType)
                    result['Node ' + i + 'Parallel'] = Common.FetchDB(sql)[0]['component']
                sql = "select GROUP_CONCAT(a.component) as component, count(*) from (select component from ICT_Project.wirelist where node in " \
                      "('{0}','{1}','{2}') and board = '{3}' and component != '{4}')a inner join " \
                      "(select item as component from ICT_Project.fail_program where board = '{3}' and item != '{4}' and create_time >= '{5}') b " \
                      "on a.component = b.component".format(result['Node 1'], result['Node 2'], result['Node 3'], board,
                                                            item, t1)
                result['Node Parallel'] = Common.FetchDB(sql)[0]['component']
                result['Parallel'] = 'None'
                result['Node'] = 'None'
                # open/short
                result['from'] = frompoint
                result['to'] = endpoint
                # boundary scan
                result['Testjet Test Result'] = 'None'
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR1": str(inst)}
        response = jsonify(result)
        response.status_code = 200
        return result


@restapi.resource('/prog/fail_analysis')
class ProgFailAnalysis(Resource):
    def get(self, headers=None):
        # 取得request參數
        uid = request.args.get('uid', '')

        h_per = 0
        l_per = 0
        hlimit = 0
        llimit = 0
        normal_value = 0

        data = []
        sql = "select last_close_time from ICT_Project.fail_program where uid = {}".format(uid)
        t1 = Common.FetchDB(sql)
        t1 = t1[0]['last_close_time']
        if not t1:
            t1 = dt.now() + timedelta(weeks=-4)

        sql = ''' SELECT program_id,board,bu,item,test_type,fail_state FROM ICT_Project.fail_program WHERE uid={} '''.format(
            uid)
        row = Common.FetchDB(sql)
        if (row):
            program_id = row[0]['program_id']
            board = row[0]['board']
            bu = row[0]['bu']
            item = row[0]['item']
            test_type = row[0]['test_type']
            fail_state = row[0]['fail_state']

        # 若是失敗結案，須找到最新open case的uid，取得量測值
        if (fail_state ==3):
            sql = ''' SELECT uid FROM ICT_Project.fail_program WHERE bu='{0}' AND program_id='{1}' AND board='{2}' AND item='{3}' 
                      AND test_type='{4}' ORDER BY create_time DESC LIMIT 1 '''.format(bu,program_id,board,item,test_type)
            uid = Common.FetchDB(sql)[0]['uid']

        sql = '''  SELECT count(*) as count,rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal from 
                   (
                        SELECT case when rate>100 then 100 when rate < -100 then -100 else rate end as rate,
                        high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal
                        FROM
                        (
                            SELECT a.measured,a.item,round((a.measured-b.nominal)/b.nominal*100) as rate,b.high_limit,b.low_limit,b.nominal,
                            case when b.conv_high_limit='' then round(b.high_limit,2) else b.conv_high_limit end as conv_high_limit,
                            case when b.conv_low_limit='' then round(b.low_limit,2) else b.conv_low_limit end as conv_low_limit,
                            case when b.conv_nominal='' then round(b.nominal,2) else b.conv_nominal end as conv_nominal
                            FROM 
                            ( 
                                SELECT measured,item FROM ICT_Project.fail_measured a WHERE case_id={0} and label_no=2
                            ) a
                            INNER JOIN 
                            (
                                SELECT item,high_limit ,low_limit,
                                case when nominal is null then (high_limit+low_limit)/2 else nominal end as nominal,
                                conv_high_limit,conv_low_limit,conv_nominal
                                from ICT_Project.fail_measured where case_id={0} AND label_no=2
                                order by case_id limit 1
                            )b on a.item=b.item 
                        ) x
                    )y group by rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal
                    UNION
                    SELECT count(*) as count,rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal from 
                   (
                        SELECT case when rate>100 then 100 when rate < -100 then -100 else rate end as rate,
                        high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal
                        FROM
                        (
                            SELECT a.measured,a.item,round((a.measured-b.nominal)/b.nominal*100) as rate,b.high_limit,b.low_limit,b.nominal,
                            case when b.conv_high_limit='' then round(b.high_limit,2) else b.conv_high_limit end as conv_high_limit,
                            case when b.conv_low_limit='' then round(b.low_limit,2) else b.conv_low_limit end as conv_low_limit,
                            case when b.conv_nominal='' then round(b.nominal,2) else b.conv_nominal end as conv_nominal
                            FROM 
                            ( 
                                SELECT measured,component as item FROM ICT_Project.pass_measured 
                                WHERE component='{1}' AND BU='{2}' AND program_id='{3}' AND board='{4}' AND test_type='{5}'
                            ) a
                            INNER JOIN 
                            (
                                SELECT item,high_limit ,low_limit,
                                case when nominal is null then (high_limit+low_limit)/2 else nominal end as nominal,
                                conv_high_limit,conv_low_limit,conv_nominal
                                from ICT_Project.fail_measured where case_id={0} AND label_no=2 
                                order by case_id limit 1
                            )b on a.item=b.item 
                        ) x
                    )y group by rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal'''.format(uid, item, bu, program_id, board, test_type)
 
        try:
            rows = Common.FetchDB(sql)
            # print(rows)
            h_per = round((rows[0]['high_limit'] - rows[0]['nominal']) / rows[0]['nominal'] * 100,2)
            l_per = round((rows[0]['low_limit'] - rows[0]['nominal']) / rows[0]['nominal'] * 100,2)
            if (test_type=='testjet'):
                hlimit = str(round(rows[0]['high_limit'],2))
                llimit = str(round(rows[0]['low_limit'],2))
                normal_value = str(round(rows[0]['nominal'],2))
            else:
                hlimit = rows[0]['conv_high_limit']
                llimit = rows[0]['conv_low_limit']
                normal_value = str(rows[0]['conv_nominal'])

            for row in rows:
                # 新增百分比與其個數list
                d = []
                d.append(int(row['rate']))
                d.append(int(row['count']))
                data.append(d)

            result = {
                'data': data,
                'h_per': str(h_per),
                'l_per': str(l_per),
                'hlimit': str(hlimit),
                'llimit': str(llimit),
                'normal_value': str(normal_value)
            }
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}
        response = jsonify(result)
        response.status_code = 200
        return result


@restapi.resource('/prog/ratio')
class ProgRatio(Resource):
    def get(self, headers=None):
        BU = request.args.get('BU', '')
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")
        op = request.args.get('op', '')
        syst = request.args.get('syst', '')

        result = []
        try:
            if op:
                sql = ''' SELECT uid,bu,program_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                          solution_1,b.solution as solution_2,c.solution as solution_3,
                          solution_memo,case when fail_state=2 then '有效' when fail_state=3 then '無效' end as is_effective,'已處理' as state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_program a 
                          left join fail_solution b on a.solution_2=b.id
                          left join fail_solution c on a.solution_3=c.id 
                          WHERE bu='{}' AND before_retest>=5 AND create_time >='{}' AND create_time <= '{}' 
                          AND maintain_op = '{}' and fail_state>1 and fail_state<4 '''.format(BU, t1, t2, op)
                op_cases = Common.FetchDB(sql)
                result = {'op_cases':op_cases}
            elif syst:
                sql1 = '''SELECT uid,bu,program_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                          '待提交' as 'state',solution_1,solution_2,solution_3,
                          solution_memo,fail_state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_program 
                          WHERE bu='{}' AND before_retest>=5 AND create_time >='{}' AND create_time <= '{}' AND fail_state = 0 '''.format(BU, t1, t2)
                unmaintained = Common.FetchDB(sql1)
                sql2 = '''SELECT uid,bu,program_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                          case when fail_state=1 then 'ongoing' when fail_state=2 then 'closed' end as 'state',
                          case when solution_1=1 then '匹配' else '不匹配'end as solution_1,b.solution as solution_2,c.solution as solution_3,
                          solution_memo,fail_state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_program a 
                          left join fail_solution b on a.solution_2=b.id
                          left join fail_solution c on a.solution_3=c.id 
                          WHERE bu='{}' AND before_retest>=5 AND create_time >='{}' AND create_time <= '{}' and fail_state > 0 and fail_state < 4 '''.format(BU, t1, t2)
                maintained = Common.FetchDB(sql2)

                sql3 = '''SELECT uid,bu,program_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                          '待签核' as 'state',solution_1,solution_2,solution_3,
                          solution_memo,fail_state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_program 
                          WHERE bu='{}' AND before_retest>=5 AND create_time >='{}' AND create_time <= '{}' AND fail_state = 4 '''.format(BU, t1, t2)

                to_be_signed = Common.FetchDB(sql3)
                result = {'unmaintained': unmaintained, 'maintained': maintained, 'to_be_signed': to_be_signed}
            else:
                sql = '''SELECT ifnull(sum(fail_state=0),0) as unmaintained, ifnull(sum(fail_state=4),0) as to_be_signed,ifnull(sum(fail_state>0 and fail_state<4),0) as maintained, 
                                         ifnull(sum(solution_1=3 AND fail_state>0 and fail_state<4),0) as matched,ifnull(sum(solution_1 !=3 AND fail_state>0 and fail_state<4),0) 
                                         as unmatched FROM ICT_Project.fail_program WHERE bu='{}' AND 
                                         before_retest>=5 AND create_time >='{}' AND create_time <= '{}' '''.format(BU,
                                                                                                                    t1,
                                                                                                                    t2)
                maintain_info = Common.FetchDB(sql)[0]

                unmaintained = float(maintain_info['unmaintained'])
                to_be_signed = float(maintain_info['to_be_signed'])
                maintained = float(maintain_info['maintained'])
                matched = float(maintain_info['matched'])
                unmatched = float(maintain_info['unmatched'])
                result.append({'unmaintained': unmaintained,
                               'to_be_signed': to_be_signed,
                               'maintained': maintained,
                               'matched': matched,
                               'unmatched': unmatched})


                sql_op = '''SELECT maintain_op, ifnull(sum(fail_state=2),0) as matched,ifnull(sum(fail_state=3),0) as unmatched FROM 
                                            ICT_Project.fail_program WHERE bu='{}' AND before_retest>=5 and fail_state in (2,3)  
                                            AND create_time >='{}' AND create_time <= '{}' GROUP BY maintain_op ORDER BY count(*) DESC '''.format(
                    BU, t1, t2)
                rows = Common.FetchDB(sql_op)
                result.append({})
                for row in rows:
                    result[1][row['maintain_op']]={'matched':float(row['matched']),'unmatched':float(row['unmatched'])}
                result = result
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result


@restapi.resource('/prog/updatefailstate')
class UpdateProgFailState(Resource):
    def post(self, headers=None):
        try:
            result = {"result":"Fail"}
            result['values'] = request.values
            result['form'] = request.form
            uid = request.values.get('uid')
            solution1 = request.values.get('solution1')
            solution2 = request.values.get('solution2')
            solution3 = request.values.get('solution3')
            failstate = request.values.get('failState')
            op = request.values.get('op')
            job_number = request.values.get('job_number')
            approver = request.values.get('approver')
            sql = '''UPDATE ICT_Project.fail_program set solution_1 = {0} ,solution_2 = {1} ,solution_3 = {2} , maintain_op='{3}' ,
                     maintain_time = NOW() , job_number = '{4}' , fail_state = 4, approver='{5}' where uid={6} '''.format(solution1, solution2, solution3, op, job_number, approver, uid)

            if (Common.WriteToDb(sql)):
                result['result'] = 'Success'

            else:
                result['result'] = 'Fail'
                result['sql'] = sql

            return result

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}


@restapi.resource('/prog/approve',methods = ['POST','GET'])
class ProgApprove(Resource):
    def post(self, headers={'content-type': 'application/json'}):
        try:
            result = {}
            items = request.form.get('items')
            items = eval(items)
            result['items'] = items
            uid_list = [i['uid'] for i in items]
            is_del = request.form.get('is_del')
            approver = request.form.get('approver')
            job_number = request.form.get('job_number')
            result['approver'] = approver
            for uid in uid_list:
                if is_del == '0':
                    sql = '''UPDATE ICT_Project.fail_program set fail_state=1,approve_time = now(),approver = '{}',job_number = '{}' where uid={} '''.format(approver,job_number,uid)
                else:
                    sql = '''UPDATE ICT_Project.fail_program set approver = null,job_number = null, approve_time = null, fail_state = 0 where uid={} '''.format(uid)
                #print(sql)
                if (Common.WriteToDb(sql)):
                    result['result'] = 'Success'

                else:
                    result['result'] = 'Fail'

            return result

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}
    def get(self,headers=None):
        approver = request.args.get('approver','')
        job_number = request.args.get('job_number', '')
        sql = "select uid,bu,test_type,item,program_id,board,before_retest,after_retest,before_total_sn,b.solution as solution1," \
              "c.solution as solution2,d.solution as solution3,DATE_FORMAT(create_time,'%Y-%m-%d %T') as create_time," \
              "DATE_FORMAT(maintain_time,'%Y-%m-%d %T') as maintain_time,maintain_op from ICT_Project.fail_program a " \
              "LEFT join ICT_Project.fail_solution b on a.solution_1=b.id " \
              "LEFT join ICT_Project.fail_solution c on a.solution_2=c.id " \
              "LEFT join ICT_Project.fail_solution d on a.solution_3=d.id " \
              "where job_number = '{}' and fail_state = 4".format(job_number)

        try:
            result = Common.FetchDB(sql)
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/prog/sn_info')
class ProgSnInfo(Resource):
    def get(self, headers=None):
        uid = request.args.get('uid', '')
        testType = request.args.get('testType', '')
        try:
            result = {}
            sql = "select last_close_time, sum(before_total_sn + after_total_sn) as total_sn from ICT_Project.fail_program where uid = {}".format(uid)
            t1 = Common.FetchDB(sql)
            total_sn = t1[0]['total_sn']
            t1 = t1[0]['last_close_time']
            if not t1:
                t1 = dt.now() + timedelta(weeks=-4)
            tb_name = ''
            if testType == 'analog':
                tb_name = 'analog_result'
            if testType == 'analog powered':
                tb_name = 'analog_powered_result'
            if testType == 'powercheck':
                tb_name == 'power_on_result'
            if tb_name:
                sql = "select distinct(sn) as sn from ICT_Project.{} where seq in (select seq from ICT_Project.fail_measured where case_id = {} and label_no = 2)".format(tb_name,uid)
                sn_list = Common.FetchDB(sql)
                result['total_sn'] = float(total_sn)
                result['fail_sn'] = float(len(sn_list))
                result['fail_sn_list'] = sn_list
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}
        response = jsonify(result)
        response.status_code = 200
        return result

