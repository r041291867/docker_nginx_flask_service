#!/usr/bin/python
# -*- coding:utf-8 -*-
import importlib,sys

from flask import Blueprint,current_app, request, make_response, jsonify
from flask_restful import Resource, Api

from . import views_blueprint
from app.extensions import mysql2,restapi,cache
from app.utils import cache_key
from app.common import Common
from flask import request
import textwrap
import gzip
import logging
import json
import decimal
import os
from datetime import date,datetime as dt, timedelta
import base64
import random
import time
import copy
import sys
import io
# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

@restapi.resource('/comp/fail_detail')
class CompFailDetail(Resource):
    def get(self, headers=None):
        result = []
        
        # 取得request參數
        BU = request.args.get('BU','')
        
        begindate = Common.BeforeNWeekDate(0)
        enddate = dt.now().strftime("%Y-%m-%d %H:%M:%S")

        sql = '''SELECT z.BU,z.board,z.sn,z.node,z.component,z.componentcode,z.vendorcode,z.datacode,z.lotcode,z.failurecode FROM ICT_Project.fail_component z
                WHERE BU= '{0}' GROUP BY z.BU,z.board,z.component,z.node'''.format(BU)          
        try:
            rows=Common.FetchDB(sql)
            # print(rows)
            # data exist
            if len(rows)>0:
                for row in rows:
                    result.append({
                        'BU': row['BU'],
                        'board': row['board'],
                        'sn': row['sn'],
                        'node': row['node'],
                        'component': row['component'],
                        'componentcode': row['componentcode'],
                        'vendorcode': row['vendorcode'],
                        'datacode': row['datacode'],
                        'lotcode': row['lotcode'],
                        'failurecode': row['failurecode']
                    })
 
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result
