#!/usr/bin/python
# -*- coding:utf-8 -*-
from flask import Blueprint
from flask_cors import CORS

views_blueprint = Blueprint('views', __name__)

CORS(views_blueprint)

from . import ICT_result
from . import ICT_Pins_result
from . import ICT_Program_result
from . import ICT_Component_result
from . import hello
