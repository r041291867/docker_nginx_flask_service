#!/usr/bin/python
# -*- coding:utf-8 -*-
import importlib,sys

from flask import Blueprint,current_app, request, make_response, jsonify
from flask_restful import Resource, Api

from . import views_blueprint
from app.extensions import mysql2,restapi,cache
from app.utils import cache_key
from app.common import Common
from flask import request
import textwrap
import gzip
import logging
import json
import decimal
import os
from datetime import date,datetime as dt, timedelta
import base64
import random
import time
import copy
import sys
import io
import numpy as np
# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')


@restapi.resource('/pins/failstatus_distribution')
class PinsFailDistribution(Resource):
    def get(self, headers=None):
        BU = request.args.get('BU', '')
        fixture_id = request.args.get('fixtureId','')
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2', '')
        listnum = request.args.get('listnum', 0)
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")
        result = []
        opening = {}
        ongoing = {}
        close = {}
        to_be_signed = {}

        sql = '''select convert(count(*), char(6)) as count, test_type, bu 
                from ICT_Project.fail_pins 
                where BU='{0}' and fixture_id='{1}'  and fail_state = 0 and create_time >='{2}' and create_time <='{3}'
                and before_retest >= {4} group by test_type '''.format(BU, fixture_id, t1, t2, listnum)

        sql2 = '''select convert(count(*), char(6)) as count, test_type, bu 
                from ICT_Project.fail_pins 
                where BU='{0}' and fixture_id='{1}'  and fail_state = 1 and create_time >='{2}' and create_time <='{3}'
                and before_retest >= {4} 
                group by test_type '''.format(BU, fixture_id, t1, t2,listnum)

        sql3 = '''select convert(count(*), char(6)) as count, test_type, bu 
                from ICT_Project.fail_pins 
                where BU='{0}' and fixture_id='{1}'  and fail_state in (2,3)  and create_time >='{2}' and create_time <='{3}'
                and before_retest >= {4} 
                group by test_type '''.format(BU, fixture_id, t1, t2,listnum)
        sql4 = '''select convert(count(*), char(6)) as count, test_type, bu 
                        from ICT_Project.fail_pins 
                        where BU='{0}' and fixture_id='{1}'  and fail_state = 4  and create_time >='{2}' and create_time <='{3}'
                        and before_retest >= {4} 
                        group by test_type '''.format(BU, fixture_id, t1, t2, listnum)
        try:
            rows = Common.FetchDB(sql)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    opening[row['test_type']] = str(row['count'])
            try:
                tmp = opening['analog']
            except:
                opening['analog'] = 0
            try:
                tmp = opening['analog powered']
            except:
                opening['analog powered'] = 0
            try:
                tmp = opening['boundary scan']
            except:
                opening['boundary scan'] = 0
            try:
                tmp = opening['powercheck']
            except:
                opening['powercheck'] = 0
            try:
                tmp = opening['testjet']
            except:
                opening['testjet'] = 0
            try:
                tmp = opening['short']
            except:
                opening['short'] = 0
            try:
                tmp = opening['open']
            except:
                opening['open'] = 0

            rows = Common.FetchDB(sql2)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    ongoing[row['test_type']] = str(row['count'])
            try:
                tmp = ongoing['analog']
            except:
                ongoing['analog'] = 0
            try:
                tmp = ongoing['analog powered']
            except:
                ongoing['analog powered'] = 0
            try:
                tmp = ongoing['boundary scan']
            except:
                ongoing['boundary scan'] = 0
            try:
                tmp = ongoing['powercheck']
            except:
                ongoing['powercheck'] = 0
            try:
                tmp = ongoing['testjet']
            except:
                ongoing['testjet'] = 0
            try:
                tmp = ongoing['short']
            except:
                ongoing['short'] = 0
            try:
                tmp = ongoing['open']
            except:
                ongoing['open'] = 0

            rows = Common.FetchDB(sql3)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    close[row['test_type']] = str(row['count'])
            try:
                tmp = close['analog']
            except:
                close['analog'] = 0
            try:
                tmp = close['analog powered']
            except:
                close['analog powered'] = 0
            try:
                tmp = close['boundary scan']
            except:
                close['boundary scan'] = 0
            try:
                tmp = close['powercheck']
            except:
                close['powercheck'] = 0
            try:
                tmp = close['testjet']
            except:
                close['testjet'] = 0
            try:
                tmp = close['short']
            except:
                close['short'] = 0
            try:
                tmp = close['open']
            except:
                close['open'] = 0
                
            rows = Common.FetchDB(sql4)
            # data exist
            if len(rows) > 0:
                for row in rows:
                    to_be_signed[row['test_type']] = str(row['count'])
            try:
                tmp = to_be_signed['analog']
            except:
                to_be_signed['analog'] = 0
            try:
                tmp = to_be_signed['analog powered']
            except:
                to_be_signed['analog powered'] = 0
            try:
                tmp = to_be_signed['boundary scan']
            except:
                to_be_signed['boundary scan'] = 0
            try:
                tmp = to_be_signed['powercheck']
            except:
                to_be_signed['powercheck'] = 0
            try:
                tmp = to_be_signed['testjet']
            except:
                to_be_signed['testjet'] = 0
            try:
                tmp = to_be_signed['short']
            except:
                to_be_signed['short'] = 0
            try:
                tmp = to_be_signed['open']
            except:
                to_be_signed['open'] = 0

            result.append(opening)
            result.append(to_be_signed)
            result.append(ongoing)
            result.append(close)
            

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result

@restapi.resource('/pins/fixture_detail')
class PinsFixtureDetail(Resource):
    def get(self, headers=None):
        # 取得request參數
        fixture_id = request.args.get('fixtureId','')
        BU = request.args.get('BU','')
        t1 = request.args.get('t1','')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")
        
        result = {}
        opening = 0
        to_be_signed = 0
        ongoing = 0
        close = 0

        # print(str(today))
        sql = '''SELECT fail_state,count(*) as case_qyt FROM ICT_Project.fail_pins WHERE fixture_id='{0}' AND BU='{1}' 
                 AND create_time BETWEEN '{2}' AND '{3}' and before_retest >= 5  
                 GROUP BY fail_state'''.format(fixture_id,BU,t1,t2)

        rows = Common.FetchDB(sql)

        if(rows):
            for row in rows:
                if (row['fail_state']==0):
                    opening = row['case_qyt']
                elif (row['fail_state']==1):
                    ongoing = row['case_qyt']
                elif (row['fail_state']==2):
                    close = row['case_qyt']
                elif (row['fail_state']==4):
                    to_be_signed = row['case_qyt']

        result['opening'] = opening
        result['to_be_signed'] = to_be_signed
        result['ongoing'] = ongoing
        result['close'] = close
                              
        
        response = jsonify(result)
        response.status_code=200
        return result

# @restapi.resource('/pins/fixture_detail')
# class PinsFixtureDetail(Resource):
#     def get(self, headers=None):
#         # 取得request參數
#         fixture_id = request.args.get('fixtureId','')
#         BU = request.args.get('BU','')
#         # BU = request.args.get('BU','')
        
#         new_BU=Common.ConvertBU(BU)

#         result = {}
#         resdict = {}
#         opening = {}
#         ongoing = {}
#         close = {}

#         # print(str(today))
#         sql = '''SELECT a.fixture_id,a.BU,b.probes_qty,c.totalsn,b.retry_target,c.retry,c.retest,
#                 count( DISTINCT ( a.BRC ) ) AS totalfail,count( DISTINCT ( a.BRC ) ) / b.probes_qty AS failrate,count( DISTINCT ( a.board ) ) AS PN_qty,
#                 IF(update_time_op,null,c.retest) AS Before_retest,
#                 IF(update_time_op,null,c.retry) AS Before_retry,
#                 IF(update_time_op,null,0) AS After_retest,
#                 IF(update_time_op,null,0) AS After_retry
#                 FROM ICT_Project.pins_fail a INNER JOIN ICT_Project.target_value b ON a.fixture_id = b.fixture_id
#                 INNER JOIN (
#                     SELECT fixture_id,sum( retest ) / count( DISTINCT sn ) AS retry,count( * ) / count( DISTINCT sn )  AS retest,
#                     count( DISTINCT sn ) AS totalsn 
#                 FROM ICT_Project.ict_detail_result -- WHERE end_time BETWEEN '{2}' AND '{3}' 
#                 WHERE fixture_id = '{0}' GROUP BY fixture_id 
#                     ) c ON a.fixture_id = c.fixture_id 
#                 WHERE a.fixture_id = '{0}' AND a.BU = '{1}' -- and a.end_time between '{2}' and '{3}'
#                 GROUP BY a.BU'''.format(fixture_id,BU,Common.BeforeNWeekDate(0),dt.now().strftime("%Y-%m-%d %H:%M:%S"))
                
#         # 計算治具狀態(ongoing,close,opening數量)
#         # sql2 = '''SELECT a.fixture_id,a.BU,b.probes_qty,COUNT(*) as fail_state_count,fail_state
#         #             FROM ICT_Project.pins_fail a
#         #             INNER JOIN ICT_Project.target_value b ON a.fixture_id=b.fixture_id and a.board=b.board and a.BU = b.BU
#         #             WHERE a.fixture_id='{0}' and a.BU = '{1}' -- and a.end_time between '{2}' and '{3}'
#         #             GROUP BY a.BU'''.format(fixture_id,BU,Common.BeforeNWeekDate(0),dt.now().strftime("%Y-%m-%d %H:%M:%S"))
#         sql2 = '''SELECT fixture_id, sum(count) as fail_state_count, fail_state FROM (
#                 SELECT case when test_type in ('open','short') then sum(failcount2) else sum(failcount) end as count,test_type,BU,fixture_id,fail_state from (
#                     select fixture_id, test_type,count(distinct component) as failcount,count(distinct BRC) as failcount2,BU,fail_state
#                     from ICT_Project.pins_fail where BU='{0}' and fixture_id='{1}'  and fail_state = 0 
#                     group by test_type,board,fixture_id,machine
#                 ) a group by test_type ) g
#                 UNION
#                 SELECT fixture_id, sum(count) as fail_state_count, fail_state FROM (
#                 SELECT case when test_type in ('open','short') then sum(failcount2) else sum(failcount) end as count,test_type,BU,fixture_id,fail_state from (
#                     select fixture_id, test_type,count(distinct component) as failcount,count(distinct BRC) as failcount2,BU,fail_state
#                     from ICT_Project.pins_fail where BU='{0}' and fixture_id='{1}'  and fail_state = 1
#                     group by test_type,board,fixture_id,machine
#                 ) a group by test_type ) g
#                 UNION
#                 SELECT fixture_id, sum(count), fail_state FROM (
#                 SELECT case when test_type in ('open','short') then sum(failcount2) else sum(failcount) end as count,test_type,BU,fixture_id,fail_state from (
#                     select fixture_id, test_type,count(distinct component) as failcount,count(distinct BRC) as failcount2,BU,fail_state
#                     from ICT_Project.pins_fail where BU='{0}' and fixture_id='{1}'  and fail_state = 2
#                     group by test_type,board,fixture_id,machine
#                 ) a group by test_type ) g'''.format(BU,fixture_id)
#         #
#         sqloptime=''' SELECT update_time_op FROM ICT_Project.pins_fail 
#                       WHERE fixture_id = '{0}' AND BU = '{1}' and update_time_op is not null 
#                       ORDER BY update_time_op ASC LIMIT 1  '''.format(fixture_id,BU)
#         row=Common.FetchDB(sqloptime)
#         if (len(row)>0):
#             updatetime=row[0]['update_time_op']
#         else:
#             updatetime=dt.now()

#         # Before retry/retset
#         sql3 = '''  SELECT b.mfg,a.fixture_id,sum( retest ) / count( DISTINCT sn ) AS Before_retry,
#                     count( * ) / count( DISTINCT sn )  AS Before_retest,count( DISTINCT sn ) AS totalsn
#                     from ICT_Project.ict_detail_result a inner join ICT_Project.tb_fixture b 
#                     on a.board=b.pn where a.fixture_id = '{0}' and b.mfg='{1}' and end_time<='{2}'   '''.format(fixture_id,BU,updatetime)
#         # After retry/retset
#         sql4 = ''' SELECT b.mfg,a.fixture_id,sum( retest ) / count( DISTINCT sn ) AS After_retry,
#                    count( * ) / count( DISTINCT sn )  AS After_retest,count( DISTINCT sn ) AS totalsn
#                    from ICT_Project.ict_detail_result a inner join ICT_Project.tb_fixture b 
#                    on a.board=b.pn where a.fixture_id = '{0}' and b.mfg='{1}' and end_time>'{2}'   '''.format(fixture_id,BU,updatetime)
#         # 計算治具中每個pn的成功與失敗數(all)
#         sql5 = '''SELECT board,status,count(*) as count FROM ict_detail_result WHERE machine LIKE '{0}' and fixture_id = '{1}' GROUP BY board,`status`
#         '''.format(new_BU,fixture_id)
#         # 計算治具中每個pn的成功與失敗數(Before)
#         sql6 = '''SELECT a.board,status,count(*) as count,b.update_time_op AS update_time FROM ICT_Project.ict_detail_result a LEFT JOIN ( 
#                 SELECT BU, board, sn, end_time, fixture_id, min(update_time_op) as update_time_op FROM ICT_Project.pins_fail 
#                 WHERE fixture_id = '{0}' AND BU = '{1}' ) b ON a.fixture_id = b.fixture_id
#                 WHERE a.end_time <= b.update_time_op and a.machine LIKE '{2}' and a.fixture_id = '{0}' GROUP BY a.board,a.`status`
#         '''.format(fixture_id,BU,new_BU)
#         # 計算治具中每個pn的成功與失敗數(After)
#         sql7 = '''SELECT a.board,status,count(*) as count,b.update_time_op AS update_time FROM ICT_Project.ict_detail_result a LEFT JOIN ( 
#                 SELECT BU, board, sn, end_time, fixture_id, min(update_time_op) as update_time_op FROM ICT_Project.pins_fail 
#                 WHERE fixture_id = '{0}' AND BU = '{1}' ) b ON a.fixture_id = b.fixture_id
#                 WHERE a.end_time > b.update_time_op and a.machine LIKE '{2}' and a.fixture_id = '{0}' GROUP BY a.board,a.`status`
#         '''.format(fixture_id,BU,new_BU)
#         try:
#             rows=Common.FetchDB(sql2)
#             if len(rows)>0:
#                 for row in rows:
#                     if row['fail_state'] == 0:
#                         opening[row['fixture_id']] = row['fail_state_count']
#                     elif row['fail_state'] == 1:
#                         ongoing[row['fixture_id']] = row['fail_state_count']
#                     elif row['fail_state'] == 2:
#                         close[row['fixture_id']] = row['fail_state_count']
#                 for row in rows:
#                     # 如果找不到key則代表沒有值 => list裡面等於0
#                     try:
#                         tmp = opening[row['fixture_id']]
#                     except:
#                         opening[row['fixture_id']] = 0
#                     try:
#                         tmp = ongoing[row['fixture_id']]
#                     except:
#                         ongoing[row['fixture_id']] = 0
#                     try:
#                         tmp = close[row['fixture_id']]
#                     except:
#                         close[row['fixture_id']] = 0
#         except Exception as inst:
#             print(inst)
#             logging.getLogger('error_Logger').error('Pins Fail Err')
#             logging.getLogger('error_Logger').error(inst)
#             return {"ERROR1": str(inst)}
#         has_op_time = False
#         try:
#             rows=Common.FetchDB(sql)
#             # print(rows)
#             # data exist
#             if len(rows)>0:
#                 for row in rows:
#                     result['fixture_id'] = row['fixture_id']
#                     result['BU'] = row['BU']
#                     result['total_probe'] = row['probes_qty']
#                     result['current_week'] = row['totalsn']
#                     result['retry_target'] = row['retry_target']
#                     result['retry_actual'] = str(round(row['retry'],2))
#                     result['retest_times'] = str(round(row['retest'],2))
#                     result['total_fail'] = row['totalfail']
#                     result['fail_rate'] = str(round(row['failrate'],2))
#                     result['opening'] = float(opening[row['fixture_id']])
#                     result['ongoing'] = float(ongoing[row['fixture_id']])
#                     result['close'] = float(close[row['fixture_id']])
#                     result['Before_uph']=0
#                     result['After_uph']=0
#                     result['boards_qty']=str(row['PN_qty'])
#                     if row['Before_retest'] is None:
#                         has_op_time = True
#                         rows2=Common.FetchDB(sql3)
#                         rows3=Common.FetchDB(sql4)
#                         if len(rows)>0:
#                             for row2 in rows2:
#                                 result['Before_retest'] = str(round(row2['Before_retest'],2))
#                                 result['Before_retry'] = str(round(row2['Before_retry'],2))
#                             for row3 in rows3:
#                                 result['After_retest'] = str(round(row3['After_retest'],2))
#                                 result['After_retry'] = str(round(row3['After_retry'],2))
#                     else:
#                         has_op_time = False
#                         result['Before_retest'] = str(round(row['Before_retest'],2))
#                         result['Before_retry'] = str(round(row['Before_retry'],2))
#                         result['After_retest'] = str(round(row['After_retest'],2))
#                         result['After_retry'] = str(round(row['After_retry'],2))

#         except Exception as inst:
#             print(inst)
#             logging.getLogger('error_Logger').error('Pins Fail Err')
#             logging.getLogger('error_Logger').error(inst)
#             return {"ERROR2": str(inst)}
#         board_info={}
#         # 計算UPH
#         if has_op_time == True:
#             result['Before_uph'] = self.cal_UPH(sql6,1,new_BU,fixture_id)
#             result['After_uph'] = self.cal_UPH(sql7,2,new_BU,fixture_id)
#         else: 
#             result['Before_uph'] = self.cal_UPH(sql5,0,new_BU,fixture_id)
#             result['After_uph'] = 0
        
#         response = jsonify(result)
#         response.status_code=200
#         return result

    # 收敛平均值
    def mean_convergence(self,mean, uph_all):
        total_log = len(uph_all)
        if total_log > 0:
            unit_time = [i[0] for i in uph_all]
            test_time = [i[1] for i in uph_all]
            handling_time = [i[0]-i[1] for i in uph_all]
            unit_mean, test_mean, handling_mean = sum(unit_time)/total_log, sum(test_time)/total_log, \
                                                  sum(handling_time)/total_log,
            if abs(mean[0]-unit_mean) > 0.001*mean[0] or abs(mean[1]-test_mean > 0.001*mean[1]) or \
                    abs(mean[2]-handling_mean) > 0.001*mean[2]:
                new_uph_all = []
                for i,j in enumerate(uph_all):
                    # if (4*j[0]<unit_mean or 3*unit_mean<j[0]) or (4*j[1]<test_mean or 3*test_mean<j[1]) \
                    #         or 2*handling_mean<j[0]-j[1]:
                    if (j[0]-j[1]<3*handling_mean) and j[0]<3*unit_mean and j[1]<3*test_mean:  # 只限制大的邊界，小的不考慮
                        # del uph_all[i]
                        new_uph_all.append(j)
                return self.mean_convergence((unit_mean, test_mean, handling_mean), new_uph_all)
        return uph_all

    def cal_UPH(self,sql,label,new_BU,fixture_id):
        rows=Common.FetchDB(sql)
        board_info = {}
        update_time = ''
        try:
            if len(rows)>0:
                for row in rows:
                    board_info[row['board']] = {}
                for row in rows:
                    try:
                        update_time = row['update_time']
                    except:
                        update_time = None
                    board_info[row['board']][row['status']] = row['count']
            else: return 0
            uph_list = []
            for board in board_info:
                sql_get = ''
                if update_time is None:
                    sql_get = '''
                    SELECT status,start_time,end_time,total_time as test_time FROM ICT_Project.ict_detail_result WHERE machine LIKE '{0}' and fixture_id = '{1}' AND board = '{2}' 
                    ORDER BY start_time
                    '''.format(new_BU,fixture_id,board)
                elif label == 1:
                    sql_get = '''
                    SELECT status,start_time,end_time,total_time as test_time FROM ICT_Project.ict_detail_result WHERE machine LIKE '{0}' and fixture_id = '{1}' AND board = '{2}' 
                    AND end_time <= '{3}' ORDER BY start_time
                    '''.format(new_BU,fixture_id,board,update_time)
                elif label == 2:
                    sql_get = '''
                    SELECT status,start_time,end_time,total_time as test_time FROM ICT_Project.ict_detail_result WHERE machine LIKE '{0}' and fixture_id = '{1}' AND board = '{2}' 
                    AND end_time > '{3}' ORDER BY start_time
                    '''.format(new_BU,fixture_id,board,update_time)
                data=Common.FetchDB(sql_get)
                uph = []
                stat,st,et,tt = [],[],[],[]       # status,start_time,end_time,test_time
                for row in data:
                    stat.append(row['status'])
                    st.append(row['start_time'])
                    et.append(row['end_time'])
                    tt.append(row['test_time'])
                for i in range(len(st)):
                    status, start_time, end_time, test_time = stat[i],st[i], et[i], float(tt[i])
                    if i > 0:
                        cell_tag = (start_time - st[i-1]).seconds  # 和上一笔记录比较开始时间判断 是否多CELL同时出LOG
                        unit_time = (end_time - et[i-1]).seconds
                        # 过滤治具编号错误导致的log时间交叉，测试时间间隔得负数的bug
                        if unit_time>test_time:
                            if cell_tag:                                           # 不為0，則不是多cell
                                if status == 'PASS':
                                    uph.append([unit_time, test_time, 1])          # pass 記1，fail記0
                                else:
                                    uph.append([unit_time, test_time, 0])
                            else:             # 當多cell時，兩筆相鄰的記錄可能結束時間會相同，也可能會相差1s左右
                                if len(uph) > 0:
                                        uph.append([uph[-1][0]-uph[-1][1]+test_time, test_time, 1 if stat[i] == 'PASS' else 0])
                uph_time = 0
                test_time = 0
                pass_log = 0
                if uph:
                    uph_result = self.mean_convergence((0,0,0), uph)  # 剔除異常時間的記錄
                    for each_log in uph_result:
                        uph_time += each_log[0]
                        if each_log[2] == 1:
                            test_time += each_log[1]
                            pass_log += 1
                if uph_time == 0:
                    return 0
                # try: tmp = board_info[board]['PASS']
                # except: return 0
                uph_value = pass_log/(uph_time/3600)
                # print(str(board_info[board]['PASS'])+"==================================="+str(uph_time/3600))
                uph_list.append(uph_value)
        except Exception as inst:
            return {"ERROR3": str(inst)}
        return float(round(sum(uph_list)/len(uph_list),2))


@restapi.resource('/pins/fail_detail')
class PinsFailDetail(Resource):
    def get(self, headers=None):
        result = []

        # 取得request參數
        BU = request.args.get('BU', '')
        weeknum = request.args.get('weeknum', '')
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")

        #num = 13 - int(weeknum.strip('W'))
        # begindate = Common.BeforeNWeekDate(num)
        # enddate = dt.now().strftime("%Y-%m-%d %H:%M:%S") if num == 0 else Common.BeforeNWeekDate(num-1)

        sql = '''SELECT a.fixture_id,a.BU,b.retry_target,b.probes_qty,c.retest,c.retry,c.totalsn,
                 d.totalfail, d.totalfail/probes_qty as failrate, a.open_cases 
                 from 
                    (select bu,fixture_id, sum(fail_state=0) as open_cases FROM ICT_Project.fail_pins where bu='{0}' and create_time>='{1}' and create_time <='{2}' and before_retest >= 5 group by fixture_id) a
                 inner join 
                    (select fixture_id,retry_target,probes_qty from ICT_Project.target_value where bu = '{0}' group by fixture_id) b 
                    on a.fixture_id=b.fixture_id 
                 inner join 
                    (
                    select fixture_id,sum(retest+1)/count(distinct sn) as retry,
                    sum(status='fail')/count(distinct sn) as retest, 
                    count(distinct sn) as totalsn from ICT_Project.ict_detail_result 
                    where fixture_id in (select fixture_id from ICT_Project.target_value where bu='{0}')
                    group by fixture_id
                    ) c 
                    on a.fixture_id=c.fixture_id
                left join
                    (select ifnull(count(distinct(node)),0) as totalfail,fixture_id from ICT_Project.fail_node where closed = 0 group by fixture_id ) d
                    on a.fixture_id=d.fixture_id
                order by a.open_cases desc
                 '''.format(BU, t1, t2)
        try:
            rows = Common.FetchDB(sql)

            # data exist
            if len(rows) > 0:
                for row in rows:
                    result.append({
                        'fixture_id': row['fixture_id'],
                        'BU': row['BU'],
                        'probes_qty': row['probes_qty'],
                        'total_sn': row['totalsn'],
                        'retry_target': row['retry_target'],
                        'retry': str(round(row['retry'], 2)),
                        'retest': str(round(row['retest'], 2)),
                        'total_fail': row['totalfail'],
                        'fail_rate': str(row['failrate']),
                        'open_cases': str(row['open_cases']),
                    })

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result

@restapi.resource('/pins/fail_rate')
class PinsFail2(Resource):
    def get(self, headers=None):

        # 取得request參數
        BU = request.args.get('BU', '')
        weeknum = request.args.get('weeknum', '')
        weeknum = weeknum.replace('W', '')
        weekday = dt.now().weekday()
        week_12_ago = date.today() - timedelta(days=weekday + 7 * 12)

        result = {}
        sql = '''SELECT test_type,COUNT(*) as count,BU FROM ICT_Project.fail_pins WHERE BU = '{0}' 
                 AND WEEKOFYEAR(create_time) ='{1}' and before_retest >= 5 GROUP BY test_type '''.format(BU, weeknum)

        try:
            rows = Common.FetchDB(sql)

            # data exist
            if len(rows) > 0:
                for row in rows:
                    result[row['test_type']] = row['count']

            try:
                short = result['short']
            except:
                result['short'] = 0
            try:
                openn = result['open']
            except:
                result['open'] = 0
            try:
                short = result['analog']
            except:
                result['analog'] = 0
            try:
                openn = result['testjet']
            except:
                result['testjet'] = 0
            try:
                short = result['boundary scan']
            except:
                result['boundary scan'] = 0
            try:
                openn = result['analog powered']
            except:
                result['analog powered'] = 0

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result


@restapi.resource('/pins/fail_trend')
class PinsFailTrend(Resource):
    def get(self, headers=None):

        # 取得request參數
        BU = request.args.get('BU', '')
        # 12周前周一的日期
        weekday = dt.now().weekday()
        week_12_ago = date.today() - timedelta(days=weekday + 7 * 12)

        result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        try:
            weeklist = Common.WeekNumList(13)

            sql = ''' SELECT COUNT(*) as count,WEEKOFYEAR(create_time) as week  FROM ICT_Project.fail_pins 
                      WHERE BU = '{0}' AND create_time >= '{1}' and before_retest >= 5 
                      group by WEEKOFYEAR(create_time)'''.format(BU, week_12_ago)
            rows = Common.FetchDB(sql)

            for week in weeklist:
                for row in rows:
                    if (row['week'] == week):
                        result[weeklist.index(week)] = row['count']

            result = result[::-1]
            weeklist = ['W' + str(i) for i in weeklist]
            result = [result, weeklist[::-1]]

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result

@restapi.resource('/pins/retry_retest_distribution')
class PinsRetryRetestDistribution(Resource):
    def get(self, headers=None):
        result = []
        target=0
        targetlist=[]
        retryactual=[0,0,0,0,0,0,0,0,0,0,0,0,0]
        retestactual=[0,0,0,0,0,0,0,0,0,0,0,0,0]

        try:
            # 取得request參數
            fixtureId = request.args.get('fixtureId','')

            enddate=Common.BeforeNWeekDate(13)
            weeklist=Common.WeekNumList(13)

            # 取得該治具的retry target
            query = '''SELECT retry_target from ICT_Project.target_value where fixture_id='{0}' limit 1 '''.format(fixtureId)

            target=Common.FetchDB(query)[0]['retry_target']

            # get total sn 
            query = '''SELECT sum(retest+1)/count(distinct sn)-1 as retry,(count(*)/count(distinct sn))-1 as retest,
                       WEEKOFYEAR(end_time) as week from ICT_Project.ict_detail_result a 
                       where a.fixture_id='{0}' and a.end_time > '{1}' group by WEEKOFYEAR(end_time)  '''.format(fixtureId,enddate)

            rows=Common.FetchDB(query)

            for week in weeklist:
                for row in rows:
                    if (row['week']==week):
                        retryactual[weeklist.index(week)]=str(round(row['retry'],2))
                        retestactual[weeklist.index(week)]=str(round(row['retest'],2))
                targetlist.append(target)
            
            weeklist = ['W'+str(i) for i in weeklist]

            result.append({                        
                        'retry target':targetlist,
                        'retry actual':retryactual[::-1],
                        'retest actual':retestactual[::-1],
                        'weeklist':weeklist[::-1]                  
                })

        except Exception as inst:
            logging.getLogger('error_Logger').error('Pins Result Err')
            logging.getLogger('error_Logger').error(inst)

        response = jsonify(result)
        response.status_code=200
        return result


@restapi.resource('/pins/retry_retest_chart')
class PinsRetryRetestChart(Resource):
    def get(self, headers=None):
        result = []
        dict_target={}
        boardlist=[]
        try:
            # 取得request參數
            fixtureId = request.args.get('fixtureId','')

            enddate=Common.BeforeNWeekDate(13)
            weeklist=Common.WeekNumList(13)

            # 取得該治具所有測試的版
            query = '''SELECT distinct(board) from ICT_Project.ict_detail_result 
                       where fixture_id='{0}' and end_time > '{1}' '''.format(fixtureId,enddate)

            rows=Common.FetchDB(query)
      
            for row in rows:
                boardlist.append(row['board'])

            # get fail count of week
            query = '''SELECT count(distinct sn)-1 as totalsn, board, WEEKOFYEAR(end_time) as week 
                       from ICT_Project.ict_detail_result 
                       where fixture_id='{0}' and end_time > '{1}' group by board,WEEKOFYEAR(end_time) '''.format(fixtureId,enddate)

            rows=Common.FetchDB(query)

            for board in boardlist:
                totalsn=[0,0,0,0,0,0,0,0,0,0,0,0,0]

                for week in weeklist:
                    for row in rows:
                        if (row['board']==board and row['week']==week):
                            totalsn[weeklist.index(week)]=str(row['totalsn'])

                result.append({    
                        board:totalsn[::-1]                             
                })
            
            weeklist = ['W'+str(i) for i in weeklist]
            result.append(weeklist[::-1])


        except Exception as inst:
            logging.getLogger('error_Logger').error('ICT Result Err')
            logging.getLogger('error_Logger').error(inst)
            return{'result':str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result        

@restapi.resource('/pins/retry_compare')
class PinsRetryCompare(Resource):
    def get(self, headers=None):
        result = []
        fixturelist=[]

        # 取得request參數
        BU = request.args.get('BU','')
        fixture_id = request.args.get('fixtureId','')
        
        f=fixture_id.split("-")
        new_fixture = f[0]+'-'+f[1]+'%'

        try:
            enddate=Common.BeforeNWeekDate(13)
            weeklist=Common.WeekNumList(13)

            # 取得該治具所有測試的版
            query = '''SELECT distinct(fixture_id) from ICT_Project.ict_detail_result where end_time > '{0}' and fixture_id like '{1}' '''.format(enddate,new_fixture)
            rows=Common.FetchDB(query)
            
            for row in rows:
                fixturelist.append(row['fixture_id'])

            # 取得該治具所有測試的版
            query = '''SELECT fixture_id,sum(retest+1)/count(distinct sn)-1 as retry,
                       WEEKOFYEAR(end_time) as week 
                       from ICT_Project.ict_detail_result   
                       where end_time > '{0}' and fixture_id like '{1}'
                       group by fixture_id,WEEKOFYEAR(end_time) '''.format(enddate,new_fixture)

            rows=Common.FetchDB(query)
      
            for fixture in fixturelist:
                retryactual=[0,0,0,0,0,0,0,0,0,0,0,0,0]

                for week in weeklist:
                    for row in rows:
                        if (row['fixture_id']==fixture and row['week']==week):
                            retryactual[weeklist.index(week)]=str(row['retry'])

                result.append({    
                    'fixtureid':fixture,
                    'retry actual':retryactual[::-1]                         
                })

            weeklist = ['W'+str(i) for i in weeklist]
            result.append(weeklist[::-1])

        except Exception as inst:
            logging.getLogger('error_Logger').error('ICT Result Err')
            logging.getLogger('error_Logger').error(inst)

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/pins/uph_compare')
class PinsUphCompare(Resource):
    def get(self, headers=None):
        result = []
        fixturelist=[]

        # 取得request參數
        BU = request.args.get('BU','')
        fixture_id = request.args.get('fixtureId','')
        new_BU=Common.ConvertBU(BU)        

        f=fixture_id.split("-")
        new_fixture = f[0]+'-'+f[1]+'%'
        try:
            enddate = Common.BeforeNWeekDate(13)
            weeklist = Common.WeekNumList(13)
            today = dt.now().strftime("%Y-%m-%d %H:%M:%S")
            # 取得該治具所有測試的版
            query = '''SELECT distinct(fixture_id) from ICT_Project.ict_detail_result where end_time > '{0}' and fixture_id like '{1}' '''.format(enddate,new_fixture)
            rows=Common.FetchDB(query)
            
            for row in rows:
                fixturelist.append(row['fixture_id'])

            for fixture in fixturelist:
                uph_week_list = []
                for i in range(13):
                    # print(i)
                    if i > 0:
                        uph_week_list.append(self.cal_UPH(new_BU,fixture,Common.BeforeNWeekDate(i),Common.BeforeNWeekDate(i-1)))
                    else:
                        uph_week_list.append(self.cal_UPH(new_BU,fixture,Common.BeforeNWeekDate(i),today))
                
                result.append({    
                    'fixtureid':fixture,
                    'uph':uph_week_list[::-1]
                })
            weeklist = ['W'+str(i) for i in weeklist]
            result.append(weeklist[::-1])

        except Exception as inst:
            logging.getLogger('error_Logger').error('ICT Result Err')
            logging.getLogger('error_Logger').error(inst)
        response = jsonify(result)
        response.status_code=200
        return result

    # 收敛平均值
    def mean_convergence(self,mean, uph_all):
        total_log = len(uph_all)
        if total_log > 0:
            unit_time = [i[0] for i in uph_all]
            test_time = [i[1] for i in uph_all]
            handling_time = [i[0]-i[1] for i in uph_all]
            unit_mean, test_mean, handling_mean = sum(unit_time)/total_log, sum(test_time)/total_log, \
                                                  sum(handling_time)/total_log,
            if abs(mean[0]-unit_mean) > 0.001*mean[0] or abs(mean[1]-test_mean > 0.001*mean[1]) or \
                    abs(mean[2]-handling_mean) > 0.001*mean[2]:
                new_uph_all = []
                for i,j in enumerate(uph_all):
                    # if (4*j[0]<unit_mean or 3*unit_mean<j[0]) or (4*j[1]<test_mean or 3*test_mean<j[1]) \
                    #         or 2*handling_mean<j[0]-j[1]:
                    if (j[0]-j[1]<3*handling_mean) and j[0]<3*unit_mean and j[1]<3*test_mean:  # 只限制大的邊界，小的不考慮
                        # del uph_all[i]
                        new_uph_all.append(j)
                return self.mean_convergence((unit_mean, test_mean, handling_mean), new_uph_all)
        return uph_all

    def cal_UPH(self,new_BU,fixture_id,start,end):
        # 計算治具中每個pn的成功與失敗數
        sql = '''SELECT board,status,count(*) as count FROM ICT_Project.ict_detail_result WHERE machine LIKE '{0}' and fixture_id = '{1}' AND end_time BETWEEN '{2}' AND '{3}' GROUP BY board,`status`
        '''.format(new_BU,fixture_id,start,end)
        rows=Common.FetchDB(sql)
        # print(sql)
        board_info = {}
        uph_list = []
        strr = ''
        try:
            if len(rows)>0:
                for row in rows:
                    board_info[row['board']] = {}
                for row in rows:
                    board_info[row['board']][row['status']] = row['count']
            else: return 0
            strr = strr + fixture_id + ' {\n'
            for board in board_info:
                sql_get = '''
                SELECT status,start_time,end_time,total_time as test_time FROM ICT_Project.ict_detail_result WHERE machine LIKE '{0}' and fixture_id = '{1}' AND board = '{2}' 
                AND end_time BETWEEN '{3}' AND '{4}' ORDER BY start_time
                '''.format(new_BU,fixture_id,board,start,end)
                data=Common.FetchDB(sql_get)
                uph = []
                stat,st,et,tt = [],[],[],[]       # status,start_time,end_time,test_time
                for row in data:
                    stat.append(row['status'])
                    st.append(row['start_time'])
                    et.append(row['end_time'])
                    tt.append(row['test_time'])
                for i in range(len(st)):
                    status, start_time, end_time, test_time = stat[i],st[i], et[i], float(tt[i])
                    if i > 0:
                        cell_tag = (start_time - st[i-1]).seconds  # 和上一笔记录比较开始时间判断 是否多CELL同时出LOG
                        unit_time = (end_time - et[i-1]).seconds
                        # 过滤治具编号错误导致的log时间交叉，测试时间间隔得负数的bug
                        if unit_time>test_time:
                            if cell_tag:                                           # 不為0，則不是多cell
                                if status == 'PASS':
                                    uph.append([unit_time, test_time, 1])          # pass 記1，fail記0
                                else:
                                    uph.append([unit_time, test_time, 0])
                            else:             # 當多cell時，兩筆相鄰的記錄可能結束時間會相同，也可能會相差1s左右
                                if len(uph) > 0:
                                        uph.append([uph[-1][0]-uph[-1][1]+test_time, test_time, 1 if stat[i] == 'PASS' else 0])
                uph_time = 0
                test_time = 0
                pass_log = 0
                if uph:
                    uph_result = self.mean_convergence((0,0,0), uph)  # 剔除異常時間的記錄
                    for each_log in uph_result:
                        uph_time += each_log[0]
                        if each_log[2] == 1:
                            test_time += each_log[1]
                            pass_log += 1
                if uph_time == 0:
                    return 0

                uph_value = pass_log/(uph_time/3600)
                uph_list.append(uph_value)

        except Exception as inst:
            return {"ERROR3": str(inst)}

        return float(round(sum(uph_list)/len(uph_list),2))



@restapi.resource('/pins/failstate_info')
class PinsFailStateInfo(Resource):
    def get(self, headers=None):
        # 取得request參數
        fixture_id = request.args.get('fixtureId','')
        board = request.args.get('board','')
        BU = request.args.get('BU','')
        item = request.args.get('component','')
        component = item
        testType=request.args.get('testType','')
        uid = request.args.get('uid', '')

        frompoint='None'
        endpoint='None'
        conv_high_limit=''
        conv_low_limit=''
        conv_nominal=''
        result = {}
        sn_count = 0

        sql = "select last_close_time from " \
              "ICT_Project.fail_pins where uid = {}".format(uid)
        t1 = Common.FetchDB(sql)
        t1 = t1[0]['last_close_time']
        if not t1:
            t1 = dt.now() + timedelta(weeks=-4)
        
        sql2 = '''SELECT count(DISTINCT(sn)) as sn_count FROM ICT_Project.ict_detail_result 
                  WHERE board = '{0}' AND fixture_id = '{1}' 
                  GROUP BY fixture_id,board'''.format(board,fixture_id)
        sn_count = Common.FetchDB(sql2)[0]['sn_count']
        if(testType=='open' or testType=='short'):
            sql3=''' SELECT from_point,end_point from ICT_Project.open_short_fail 
                     WHERE (from_point='{0}' OR end_point='{0}') AND board='{1}' AND fail_type='{2}'
                     ORDER BY end_time desc LIMIT 1 '''.format(component,board,testType)
            point=Common.FetchDB(sql3)
            frompoint=point[0]['from_point']
            endpoint=point[0]['end_point']
        sql_node = "select GROUP_CONCAT(node) as node from ICT_Project.wirelist where board = '{0}' and test_type = '{1}' and component = '{2}' and node not in (SELECT fixed_node from ICT_Project.board_fixed_node where board='{0}') limit 3".format(board, testType, item)
        node_list = Common.FetchDB(sql_node)[0]['node']
        result['Node 1'] = 'None'
        result['Node 2'] = 'None'
        result['Node 3'] = 'None'

        try:
            sql = '''SELECT board,test_type as fail_type,item,uid,
                     before_retest+after_retest AS total_component,datacode,lotcode,round(cpk,2) as cpk
                     FROM ICT_Project.fail_pins 
                     WHERE uid = {}'''.format(uid)
            rows=Common.FetchDB(sql)
            # data exist
            if (rows):
                row=rows[0]
                uid=row['uid']
                sqllimit = ''' SELECT CASE WHEN test_type='analog' THEN conv_high_limit ELSE round(high_limit,2) END AS high_limit,
                                      CASE WHEN test_type='analog' THEN conv_low_limit ELSE round(low_limit,2) END AS  low_limit,
                                      CASE WHEN test_type='analog' THEN conv_nominal ELSE round(nominal,2) END AS nominal 
                               FROM ICT_Project.fail_measured WHERE case_id={0} AND label_no=3 ORDER BY end_time LIMIT 1'''.format(uid)
                limit = Common.FetchDB(sqllimit)
                if (limit):
                    conv_high_limit=limit[0]['high_limit']
                    conv_low_limit=limit[0]['low_limit']
                    conv_nominal=limit[0]['nominal']
                result['pn'] = row['board']
                result['fail_type'] = row['fail_type']
                result['component'] = row['item']
                result['high_limit'] = conv_high_limit
                result['low_limit'] = conv_low_limit
                result['nominal'] = conv_nominal
                result['total_component_fail'] = row['total_component']
                result['BOM'] = 0
                result['Data code'] = row['datacode'] if row['datacode'] else 'None'
                result['Lot code'] = row['lotcode'] if row['lotcode'] else 'None'
                # result['used_component_count'] = 0
                result['I Bus'] = 'None'
                result['G Bus'] = 'None'
                result['S Bus'] = 'None'
                result['Use Qty(Bot)'] = 'None'
                result['Use Qty(Top)'] = 'None'
                result['cpk'] = str(row['cpk'])
                result['yield'] = str(round(row['total_component']/sn_count,2)*100)+'%' if not row['cpk'] else 'None'
                if node_list:
                    for i,node in enumerate(node_list.split(',')):
                        result['Node ' + str(i+1)] = node

                for i in ['1','2','3']:
                    sql = "SELECT GROUP_CONCAT(component) as component,count(*) from ICT_Project.wirelist where node ='{0}' and board = '{1}' and component != '{2}' and test_type = '{3}'".format(result['Node '+ i], board, item, testType)
                    result['Node ' + i + 'Parallel'] =Common.FetchDB(sql)[0]['component']
                sql = "select GROUP_CONCAT(a.component) as component, count(*) from (select component from ICT_Project.wirelist where node in " \
                      "('{0}','{1}','{2}') and board = '{3}' and component != '{4}')a inner join " \
                      "(select item as component from ICT_Project.fail_pins where board = '{3}' and item != '{4}' and create_time >= '{5}') b " \
                      "on a.component = b.component".format(result['Node 1'], result['Node 2'], result['Node 3'], board, item, t1)
                result['Node Parallel'] = Common.FetchDB(sql)[0]['component']
                result['Parallel'] = 'None'
                result['Node'] = 'None'
                # open/short
                result['from']=frompoint
                result['to']=endpoint
                # boundary scan
                result['Testjet Test Result']='None'
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR1": str(inst)}
        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/pins/failnode')
class PinsFailNode(Resource):
    def get(self, headers=None):
        result = []
        board=''
        try:
            # 取得request參數
            fixtureId = request.args.get('fixtureId','')

            # 取得該治具board
            query = ''' SELECT * FROM
                        (
                            SELECT a.node,a.BRC,a.x,a.y,sum(b.before_retest+b.after_retest) AS retest FROM
                            (
                                SELECT node,BRC,x,y,case_id,fixture_id FROM ICT_Project.fail_node WHERE fixture_id='{0}' AND closed=0 AND node != 'GND'
                            ) a
                            INNER JOIN ICT_Project.fail_pins b ON a.fixture_id=b.fixture_id AND a.case_id=b.uid AND b.fail_state in (0,1,4)
                            GROUP BY node,BRC,x,y
                        )a  ORDER BY retest DESC '''.format(fixtureId)
            
            rows=Common.FetchDB(query)
            # data exist
            if (rows):
                for row in rows:
                    result.append({
                        'node': row['node'],
                        'x': row['x'],
                        'y': row['y'],
                        'BRC': row['BRC'],
                        'retest times': float(row['retest'])
                    })
        except Exception as inst:
            logging.getLogger('error_Logger').error('ICT Result Err')
            logging.getLogger('error_Logger').error(inst)

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/pins/passnode')
class PinsPassNode(Resource):
    def get(self, headers=None):
        result = []
        board=''
        try:
            # 取得request參數
            fixtureId = request.args.get('fixtureId','')

            # 取得該治具board
            sql = '''SELECT board from ICT_Project.fail_pins where fixture_id='{0}' limit 1  '''.format(fixtureId)
            rows=Common.FetchDB(sql)
            board=rows[0]['board']
            
            query = '''SELECT node,x,y,pins as BRC from ICT_Project.fixture where pins NOT in
                       (   
                            SELECT BRC FROM ICT_Project.fail_node WHERE fixture_id='{0}' AND closed=0 AND node != 'GND'
                       ) and board='{1}' and node != 'GND' '''.format(fixtureId,board)

            # print(query)
            rows=Common.FetchDB(query)

            # data exist
            if len(rows)>0:
                for row in rows:
                    result.append({
                        'node': row['node'],
                        'x': row['x'],
                        'y': row['y'],
                        'BRC': row['BRC']
                    })
        except Exception as inst:
            logging.getLogger('error_Logger').error('ICT Result Query Err')
            logging.getLogger('error_Logger').error(inst)
            print(inst)

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/pins/allnode')
class PinsAllNode(Resource):
    def get(self, headers=None):
        result = []
        board=''

        # 取得request參數
        fixtureId = request.args.get('fixtureId','')

        # 取得該治具board
        sql = '''SELECT board from ICT_Project.fail_pins where fixture_id='{0}' limit 1  '''.format(fixtureId)
        rows=Common.FetchDB(sql)
        board=rows[0]['board']

        
        sql = '''SELECT x,y,node from ICT_Project.fixture  
                 where board='{0}' 
                 group by x,y,node '''.format(board)

        try:
            rows=Common.FetchDB(sql)
            # data exist
            if len(rows)>0:
                for row in rows:
                    result.append({
                        'node': row['node'],
                        'x': row['x'],
                        'y': row['y']
                    })
        except Exception as inst:
            logging.getLogger('error_Logger').error('ICT Result Err')
            logging.getLogger('error_Logger').error(inst)

        response = jsonify(result)
        response.status_code=200
        return result


@restapi.resource('/pins/failstate_detail')
class PinsFailStateDetail(Resource):
    def get(self, headers=None):
        result = {}

        # 取得request參數
        BU = request.args.get('BU','')
        fixtureId = request.args.get('fixtureId','')
        testType = request.args.get('testType','')                
        failState = request.args.get('failState','') 
        listnum = request.args.get('listnum','')
        if listnum == '' : listnum = 0
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2', '')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")
        fail_state=''
        try:
            if failState == '0' :
                fail_state='=0'
            if failState == '1' :
                fail_state='=1'
            if failState == '2' :
                fail_state='in(2,3)'
            if failState == '4' :
                fail_state='=4'

            sql = ''' SELECT uid,item,fixture_id,board,before_retest,after_retest,before_total_sn,
                      case when {2}=0 then '待提交' 
                      when {2}=4 then '待签核'
                      when {2}=1 then '處理中'
                      when {2}=2 then '已完结' end as state ,
                      after_total_sn,total_times,b.solution as solution1,c.solution as solution2,d.solution as solution3,a.maintain_op,
                      case when {2}=0 then DATE_FORMAT(a.label_time,"%Y-%m-%d %T")
                      else DATE_FORMAT(a.maintain_time,"%Y-%m-%d %T") end as time, approver 
                      FROM ICT_Project.fail_pins a
                      LEFT join ICT_Project.fail_solution b on a.solution_1=b.id 
                      LEFT join ICT_Project.fail_solution c on a.solution_2=c.id 
                      LEFT join ICT_Project.fail_solution d on a.solution_3=d.id 
                      WHERE BU='{0}' AND fixture_id='{1}'  AND fail_state {7}
                      AND test_type = '{3}' AND create_time >='{4}' AND create_time <='{5}' AND before_retest >={6}
                      ORDER BY before_retest DESC '''.format(BU,fixtureId,failState,testType, t1, t2, listnum,fail_state)

            result = Common.FetchDB(sql)


        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result

@restapi.resource('/pins/updatefailstate')
class UpdatePinsFailState(Resource):
    def post(self, headers=None):
        try:
            result = {"result":"Fail"}
            result['values'] = request.values
            result['form'] = request.form
            uid = request.values.get('uid')
            solution1 = request.values.get('solution1')
            solution2 = request.values.get('solution2')
            solution3 = request.values.get('solution3')
            failstate = request.values.get('failState')
            op = request.values.get('op')
            job_number = request.values.get('job_number')
            approver = request.values.get('approver')
            sql = '''UPDATE ICT_Project.fail_pins set solution_1 = {0} ,solution_2 = {1} ,solution_3 = {2} , maintain_op='{3}' ,
                     maintain_time = NOW() , job_number = '{4}' , fail_state = 4, approver='{5}' where uid={6} '''.format(solution1, solution2, solution3, op, job_number, approver, uid)

            if (Common.WriteToDb(sql)):
                result['result'] = 'Success'

            else:
                result['result'] = 'Fail'
                result['sql'] = sql

            return result

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

@restapi.resource('/pins/approve',methods = ['POST','GET'])
class Approve(Resource):
    def post(self, headers={'content-type': 'application/json'}):
        try:
            result = {}
            #result['json'] = request.json()
            result['form'] = request.form
            result['values'] = request.values
            items = request.form.get('items')
            items = eval(items)
            result['items'] = items
            #result['json'] = json.loads(items)
            uid_list = [i['uid'] for i in items]
            #uid_list = []
            is_del = request.form.get('is_del')
            result['is_del'] = is_del
            approver = request.form.get('approver')
            job_number = request.form.get('job_number')
            result['approver'] = approver
            for uid in uid_list:
                #print(is_del)
                if is_del == '0':
                    sql = '''UPDATE ICT_Project.fail_pins set fail_state=1,approve_time = now(),approver = '{}',job_number = '{}' where uid={} '''.format(approver,job_number,uid)
                else:
                    sql = '''UPDATE ICT_Project.fail_pins set approver = null,job_number = null, approve_time = null, fail_state = 0 where uid={} '''.format(uid)
                #print(sql)
                if (Common.WriteToDb(sql)):
                    result['result'] = 'Success'

                else:
                    result['result'] = 'Fail'

            return result

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}
    def get(self,headers=None):
        approver = request.args.get('approver','')
        job_number = request.args.get('job_number', '')
        sql = "select uid,bu,test_type,item,fixture_id,board,before_retest,after_retest,before_total_sn,b.solution as solution1," \
              "c.solution as solution2,d.solution as solution3,DATE_FORMAT(create_time,'%Y-%m-%d %T') as create_time," \
              "DATE_FORMAT(maintain_time,'%Y-%m-%d %T') as maintain_time,maintain_op from ICT_Project.fail_pins a " \
              "LEFT join ICT_Project.fail_solution b on a.solution_1=b.id " \
              "LEFT join ICT_Project.fail_solution c on a.solution_2=c.id " \
              "LEFT join ICT_Project.fail_solution d on a.solution_3=d.id " \
              "where job_number = '{}' and fail_state = 4".format(job_number)

        try:
            result = Common.FetchDB(sql)
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code=200
        return result


@restapi.resource('/pins/fail_analysis')
class PinsFailAnalysis(Resource):
    def get(self, headers=None):
        print(dt.now())
        print('start')
        # 取得request參數
        uid = request.args.get('uid', '')

        h_per = 0
        l_per = 0
        hlimit = 0
        llimit = 0
        normal_value = 0

        data = []

        sql = "select last_close_time from ICT_Project.fail_pins where uid = {}".format(uid)
        t1 = Common.FetchDB(sql)
        t1 = t1[0]['last_close_time']
        if not t1:
            t1 = dt.now() + timedelta(weeks=-4)

        sql = ''' SELECT fixture_id,board,bu,item,test_type,fail_state FROM ICT_Project.fail_pins WHERE uid={} '''.format(uid)
        row = Common.FetchDB(sql)
        if (row):
            fixture_id = row[0]['fixture_id']
            board = row[0]['board']
            bu = row[0]['bu']
            item = row[0]['item']
            test_type = row[0]['test_type']
            fail_state= row[0]['fail_state']

        # 若是失敗結案，須找到最新open case的uid，取得量測值
        if (fail_state=='3'):
            sql = ''' SELECT uid FROM ICT_Project.fail_pins WHERE bu='{0}' AND fixture_id='{1}' AND board='{2}' AND item='{3}' 
                      AND test_type='{4}' ORDER BY create_time DESC LIMIT 1 '''.format(bu,fixture_id,board,item,test_type)
            uid = Common.FetchDB(sql)[0]['uid']
        print(dt.now())
        print('sql start')            
        sql = '''  SELECT count(*) as count,rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal from 
                   (
                        SELECT case when rate>100 then 100 when rate < -100 then -100 else rate end as rate,
                        high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal
                        FROM
                        (
                            SELECT a.measured,a.item,round((a.measured-b.nominal)/b.nominal*100) as rate,b.high_limit,b.low_limit,b.nominal,
                            case when b.conv_high_limit='' then round(b.high_limit,2) else b.conv_high_limit end as conv_high_limit,
                            case when b.conv_low_limit='' then round(b.low_limit,2) else b.conv_low_limit end as conv_low_limit,
                            case when b.conv_nominal='' then round(b.nominal,2) else b.conv_nominal end as conv_nominal
                            FROM 
                            ( 
                                SELECT measured,item FROM ICT_Project.fail_measured a WHERE case_id={0} and label_no=3 
                            ) a
                            INNER JOIN 
                            (
                                SELECT item,high_limit ,low_limit,
                                case when nominal is null then (high_limit+low_limit)/2 else nominal end as nominal,
                                conv_high_limit,conv_low_limit,conv_nominal
                                from ICT_Project.fail_measured where case_id={0} AND label_no=3  
                                order by case_id limit 1
                            )b on a.item=b.item 
                        ) x
                    )y group by rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal
                    UNION
                     SELECT count(*) as count,rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal from 
                   (
                        SELECT case when rate>100 then 100 when rate < -100 then -100 else rate end as rate,
                        high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal
                        FROM
                        (
                            SELECT a.measured,a.item,round((a.measured-b.nominal)/b.nominal*100) as rate,b.high_limit,b.low_limit,b.nominal,
                            case when b.conv_high_limit='' then round(b.high_limit,2) else b.conv_high_limit end as conv_high_limit,
                            case when b.conv_low_limit='' then round(b.low_limit,2) else b.conv_low_limit end as conv_low_limit,
                            case when b.conv_nominal='' then round(b.nominal,2) else b.conv_nominal end as conv_nominal
                            FROM 
                            ( 
                                SELECT measured,component as item FROM ICT_Project.pass_measured 
                                WHERE component='{1}' AND BU='{2}' AND fixture_id='{3}' AND board='{4}' AND test_type='{5}'
                            ) a
                            INNER JOIN 
                            (
                                SELECT item,high_limit ,low_limit,
                                case when nominal is null then (high_limit+low_limit)/2 else nominal end as nominal,
                                conv_high_limit,conv_low_limit,conv_nominal
                                from ICT_Project.fail_measured where case_id={0} AND label_no=3  
                                order by case_id limit 1
                            )b on a.item=b.item 
                        ) x
                    )y group by rate,high_limit,low_limit,nominal,conv_high_limit,conv_low_limit,conv_nominal'''.format(
            uid, item, bu, fixture_id, board, test_type,t1)
                   
        try:
            rows = Common.FetchDB(sql)
            print(dt.now())
            print('sql end') 

            # print(sql)
            h_per = round((rows[0]['high_limit'] - rows[0]['nominal']) / rows[0]['nominal'] * 100,2)
            l_per = round((rows[0]['low_limit'] - rows[0]['nominal']) / rows[0]['nominal'] * 100,2)
            
            if (test_type=='testjet'):
                hlimit = str(round(rows[0]['high_limit'],2))
                llimit = str(round(rows[0]['low_limit'],2))
                normal_value = str(round(rows[0]['nominal'],2))
            else:
                hlimit = rows[0]['conv_high_limit']
                llimit = rows[0]['conv_low_limit']
                normal_value = str(rows[0]['conv_nominal'])
            print(dt.now())
            print('before loop')
            for row in rows:
                # 新增百分比與其個數list
                d = []
                d.append(int(row['rate']))
                d.append(int(row['count']))
                data.append(d)

            result = {
                'data': data,
                'h_per': str(h_per),
                'l_per': str(l_per),
                'hlimit': str(hlimit),
                'llimit': str(llimit),
                'normal_value': str(normal_value)
            }
            print(dt.now())
            print('End')

        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Prog Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}
        response = jsonify(result)
        response.status_code = 200
        return result

@restapi.resource('/pins/ratio')
class PinsRatio(Resource):
    def get(self, headers=None):
        BU = request.args.get('BU', '')
        t1 = request.args.get('t1', '')
        t2 = request.args.get('t2','')
        t2 = (dt.strptime(t2, "%Y/%m/%d")+timedelta(days=1)).strftime("%Y/%m/%d")
        op = request.args.get('op', '')
        syst = request.args.get('syst', '')

        result = []
        try:
            if op:
                sql = ''' SELECT uid,bu,fixture_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                          solution_1,b.solution as solution_2,c.solution as solution_3,
                          solution_memo,case when fail_state=2 then '有效' when fail_state=3 then '無效' end as is_effective,'已處理' as state,
                          maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_pins a 
                          left join fail_solution b on a.solution_2=b.id 
                          left join fail_solution c on a.solution_3=c.id 
                          WHERE bu='{}' AND before_retest>=5 
                          AND create_time >='{}' AND create_time <= '{}' AND maintain_op = '{}' and fail_state>1 and fail_state<4'''.format(BU, t1, t2, op)
                op_cases = Common.FetchDB(sql)
                result = {'op_cases':op_cases}
            elif syst:
                sql1 = ''' SELECT uid,bu,fixture_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                           '待提交' as 'state',solution_1,solution_2,solution_3,
                           solution_memo,fail_state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                           label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                           after_retest,after_total_sn,total_times FROM ICT_Project.fail_pins 
                           WHERE bu='{}' AND before_retest>=5 
                           AND create_time >='{}' AND create_time <= '{}' AND fail_state = 0 '''.format(BU, t1, t2)
                unmaintained = Common.FetchDB(sql1)
                sql2 = '''SELECT uid,bu,fixture_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                          case when fail_state=1 then 'ongoing' when fail_state=2 then 'closed' end as 'state',
                          case when solution_1=1 then '匹配' else '不匹配'end as solution_1,b.solution as solution_2,c.solution as solution_3,
                          solution_memo,fail_state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_pins a 
                          left join fail_solution b on a.solution_2=b.id 
                          left join fail_solution c on a.solution_3=c.id 
                          WHERE bu='{}' AND before_retest>=5 
                          AND create_time >='{}' AND create_time <= '{}' AND fail_state > 0 and fail_state < 4 '''.format(BU, t1, t2)
                maintained = Common.FetchDB(sql2)
                sql3 = ''' SELECT uid,bu,fixture_id,board,item,cast(cpk as char) as cpk,cast(end_time as char) as end_time,test_type,
                                          '待签核' as state,
                                          solution_1,b.solution as solution_2,solution_3,
                                          solution_memo,fail_state,maintain_op,cast(maintain_time as char) as maintain_time,cast(close_time as char) as close_time,
                                          label_no,new_label_no,cast(label_time as char) as label_time,cast(create_time as char) as create_time,before_retest,before_total_sn,
                                          after_retest,after_total_sn,total_times FROM ICT_Project.fail_pins a 
                                          left join fail_solution b on a.solution_2=b.id WHERE bu='{}' AND before_retest>=5 
                                           AND create_time >='{}' AND create_time <= '{}' AND fail_state = 4 '''.format(
                    BU, t1, t2)
                to_be_signed = Common.FetchDB(sql3)
                result = {'unmaintained':unmaintained, 'maintained':maintained, 'to_be_signed':to_be_signed}
            else:
                sql = '''SELECT ifnull(sum(fail_state=0),0) as unmaintained, ifnull(sum(fail_state=4),0) as to_be_signed,ifnull(sum(fail_state>0 and fail_state<4),0) as maintained, 
                         ifnull(sum(solution_1=1 AND fail_state>0 and fail_state<4),0) as matched,ifnull(sum(solution_1 !=1 AND fail_state>0 and fail_state<4),0) 
                         as unmatched FROM ICT_Project.fail_pins WHERE bu='{}' AND 
                         before_retest>=5 AND create_time >='{}' AND create_time <= '{}' '''.format(BU, t1, t2)
                #print(sql)               
                maintain_info = Common.FetchDB(sql)[0]
                #print(maintain_info)
                unmaintained = float(maintain_info['unmaintained'])
                to_be_signed = float(maintain_info['to_be_signed'])
                maintained = float(maintain_info['maintained'])
                matched = float(maintain_info['matched'])
                unmatched = float(maintain_info['unmatched'])
                result.append({'unmaintained':unmaintained,
                               'to_be_signed':to_be_signed,
                               'maintained':maintained,
                               'matched':matched,
                               'unmatched':unmatched})

                sql_op = '''SELECT maintain_op, ifnull(sum(fail_state=2),0) as matched,ifnull(sum(fail_state=3),0) as unmatched FROM 
                            ICT_Project.fail_pins WHERE bu='{}' AND before_retest>=5 and fail_state in (2,3)  
                            AND create_time >='{}' AND create_time <= '{}' GROUP BY maintain_op ORDER BY count(*) DESC '''.format(BU, t1, t2)
                #print(sql_op)
                rows = Common.FetchDB(sql_op)
                #print(rows)
                result.append({})
                for row in rows:
                    result[1][row['maintain_op']]={'matched':float(row['matched']),'unmatched':float(row['unmatched'])}
                #print(result)
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}

        response = jsonify(result)
        response.status_code = 200
        return result


@restapi.resource('/pins/sn_info')
class PinsSnInfo(Resource):
    def get(self, headers=None):
        uid = request.args.get('uid', '')
        fixture = request.args.get('fixtureId', '')
        pn = request.args.get('pn', '')
        testType = request.args.get('testType', '')
        try:
            result = {}
            sql = "select last_close_time, sum(before_total_sn + after_total_sn) as total_sn from ICT_Project.fail_pins where uid = {}".format(uid)
            t1 = Common.FetchDB(sql)
            total_sn = t1[0]['total_sn']
            t1 = t1[0]['last_close_time']
            if not t1:
                t1 = dt.now() + timedelta(weeks=-4)
            tb_name = ''
            if testType == 'analog':
                tb_name = 'analog_result'
            if testType == 'analog powered':
                tb_name = 'analog_powered_result'
            if testType == 'powercheck':
                tb_name == 'power_on_result'
            if tb_name:
                sql = "select distinct(sn) as sn from ICT_Project.{} where seq in (select seq from ICT_Project.fail_measured where case_id = {} and label_no = 3)".format(tb_name,uid)
                sn_list = Common.FetchDB(sql)
                result['total_sn'] = float(total_sn)
                result['fail_sn'] = float(len(sn_list))
                result['fail_sn_list'] = sn_list
        except Exception as inst:
            print(inst)
            logging.getLogger('error_Logger').error('Pins Fail Err')
            logging.getLogger('error_Logger').error(inst)
            return {"ERROR": str(inst)}
        response = jsonify(result)
        response.status_code = 200
        return result
