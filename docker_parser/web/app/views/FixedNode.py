import flask_restful

from flask_restful import request
from werkzeug.datastructures import FileStorage
from flask import Flask
from flask_restful import Resource, Api, reqparse
from app.extensions import mysql2,restapi,cache
import time
import logging
from datetime import date,datetime as dt, timedelta
import codecs, hashlib, os, shutil


app = Flask(__name__)
api = Api(app=app)

@restapi.resource('/board/<string:filename>')
class FixedNode(Resource):
	def get(self,filename):
		path=os.path.abspath("..")+"board/"

		fp = open(path+filename, 'r')

		line = fp.readline()
		board = filename

		while line :
			sp=line.split()
			if (len(sp)>0 and sp[0] == 'FIXED' and sp[1] == 'NODE'):
				# print(sp[])
				fixed_node=''
				db_sp=[]
				db_sp.append(board)
				line=fp.readline()
				while line:
					# print(line)
					sp = line.replace(';',' ').split()
					
					if (len(sp)>0):
						if(sp[0]=='GND' and sp[1]=='GROUND'):break
						elif(len(sp)>1 and sp[1]=='Family' and sp[2]=='ALL'):
							fixed_node=sp[0]
							db_sp.append(fixed_node)
							if(db_sp[0]!='' and db_sp[1]!=''):print(db_sp)
							db_sp.pop()
						line=fp.readline()

			line = fp.readline()

		fp.close()
		return {'hello': 'world'}
	"""
	数据接口
	"""
	def __init__(self):
		self.parser = reqparse.RequestParser()
		self.parser.add_argument('file', required=True, type=FileStorage, location='files')

	def post(self,filename):
		result = {"result":"Fail"}
		WriteDbResult = False
		
		try:
			path=os.path.abspath("..")+"board/"

			fp = open(path+filename, 'r')

			line = fp.readline()
			board = filename

			while line :
				sp=line.split()
				if (len(sp)>0 and sp[0] == 'FIXED' and sp[1] == 'NODE'):
					# print(sp[])
					fixed_node=''
					db_sp=[]
					db_sp.append(board)
					line=fp.readline()
					while line:
						# print(line)
						sp = line.replace(';',' ').split()
						
						if (len(sp)>0):
							if(sp[0]=='GND' and sp[1]=='GROUND'):break
							elif(len(sp)>1 and sp[1]=='Family' and sp[2]=='ALL'):
								fixed_node=sp[0]
								db_sp.append(fixed_node)
								if(db_sp[0]!='' and db_sp[1]!=''):WriteDbResult=self.WriteToDb(db_sp)
								db_sp.pop()
							line=fp.readline()

				line = fp.readline()

			fp.close()

		except Exception as err:
			print("[error]: {0}".format(err))
		
		if WriteDbResult :
			result['result'] = 'Success'
		return result

	def WriteToDb(self,lists):
		
		Items = 'insert ignore into ICT_Project_Test_Realtime.board_fixed_node(board,fixed_node) values ('
		
		for item in lists:
			if str(item)=="None":Items=Items+'null'+','
			else : Items = Items + '"' + str(item) + '"' + ','
		Items = Items.strip(',')
		Items += ')'
		# print (Items)
		try :
			conn = mysql2.connect()
			cursor = conn.cursor()
			cursor.execute(Items)
			conn.commit()
			cursor.close()
			conn.close()
			return True	
		except Exception as inst:
			print('ICT Test Data MySql Write Err:'+Items)
			logging.getLogger('error_Logger').error('FulearnV4 Test Data MySql Write Err:'+Items)
			logging.getLogger('error_Logger').error(inst)
			with codecs.open('./Log/ErrPost/fixednode_{0}.sql'.format(dt.now().strftime('%Y%m%d')),'ab', "utf-8") as ErrOut :
				ErrOut.write(Items)
				ErrOut.write('\n')
				ErrOut.close()
		return False


		
api.add_resource(FixedNode, '/fixednode/')


if __name__ == '__main__':
	app.run(debug=True)