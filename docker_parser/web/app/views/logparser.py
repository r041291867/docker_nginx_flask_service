import flask_restful

from flask_restful import request
from werkzeug.datastructures import FileStorage
from flask import Flask
from flask_restful import Resource, Api, reqparse
from app.extensions import mysql2,restapi,cache
import time
import logging
from datetime import date,datetime as dt, timedelta
import codecs, hashlib, os, shutil
from app.utils import cache_key
from app.myLog import MyLog

app = Flask(__name__)
api = Api(app=app)
mylog=MyLog('LogParser')

@restapi.resource('/logparser')
# @restapi.resource('/Data')
class LogParser(Resource):

	def get(self):
		try:
			mylog.logger.info(filename+'-Received')
			# file = request.files['file']
			# filename=file.filename
			# path=os.path.abspath("..")
			# fp = open(path+filename, 'r')
			result={}
			result["result"] = filename+" Fail"
			# check if file exist 
			if(filename==''):raise Exception("No file.")

			# check duplicated
			# if (self.CheckRepeat):raise Exception(filename+" is duplicated")

			# set file path
			path=os.path.abspath("..")+"/log/"
			# save file 
			# file.save(os.path.join(path,filename))
			# open file
			fp = open(path+filename, "r")

			# check filesiz
			filesize=os.stat(path+filename).st_size
			if (filesize==0):raise Exception(filename+"file size should not be 0")

			line = fp.readline()
			machine = ''
			sn_code = ''		
			EndTime = ''
			board = ''
			db_sp=[]
			db_sp_detail=[]
			fixture_id=''
			program_id=''
			isAnalogPowered=False
			

			# newlog parser
			while line :
				i=0
				line = line.strip('{\n}')
				sp=line.split(":")
				if (len(sp)==0 or ("=" in sp[0])):break
				else :
					i=i+1
					db_sp_detail.append(filename)
					if (i==4 and sp[0]!='Cell_No'):db_sp_detail.append(1)
					if (sp[0].strip() == 'Board'):
						board=sp[1].split()[0].strip()
						db_sp_detail.append(board)
					elif (sp[0].strip() == 'Serial_Number'):
						sn_code=sp[1].strip()
						db_sp_detail.append(sn_code)
					elif (sp[0].strip() == 'ICT_STATION'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Cell_No'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Status'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Fail_code'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Start_time'):
						strtime=(sp[1]+sp[2]+sp[3]).strip().replace(' ','')
						start_time=dt.strptime(strtime.replace('-',''),'%Y%m%d%H%M%S')
						db_sp_detail.append(start_time)
					elif (sp[0].strip() == 'End_time'):
						strtime=(sp[1]+sp[2]+sp[3]).strip().replace(' ','')
						EndTime=dt.strptime(strtime.replace('-',''),'%Y%m%d%H%M%S')
						db_sp_detail.append(EndTime)
					elif (sp[0].strip() == 'Test_time'):db_sp_detail.append(sp[1].strip())				
					elif (sp[0].strip() == 'Operator'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Machine'):
						machine=sp[1].strip()
						db_sp_detail.append(machine)

					elif (sp[0].strip() == 'IP_ADDRESS'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Mac_Address'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Fixture_ID'):
						# db_sp.append(sp[1].strip())
						fixture_id_list=sp[1].strip().split("-")
						fixture_id=fixture_id_list[0]+'-'+fixture_id_list[1]+'-'+fixture_id_list[2]
						db_sp_detail.append(fixture_id)
					elif (sp[0].strip() == 'Program_ID'):
						program_id=sp[1].strip()
						db_sp_detail.append(program_id)
					elif (sp[0].strip() == 'Retest'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Average_vacuum'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'MIN_vacuum'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'MAX_vacuum'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Tester_TEMP'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'ESD_IMPEDANCE'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'ESD_VOLTAGE'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation1'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation2'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation3'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation4'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation5'):
						db_sp_detail.append(sp[1].strip())

				line = fp.readline()

			# 如果db_sp_detail是空的，回傳檔案內容無法被剖析
			if(len(db_sp_detail)==0):raise Exception(filename+" fail content can't be parsed")
			line = fp.readline()

			# full log parser
			while line:
				if(line[:7]=='{@BATCH'):
					while line:
						if (line == '}\n' or line == '}}'): line = fp.readline()
						else :
							line = line.strip('{@\n}')
							sp = line.split("|")
							db_sp = [] 	
							
							while '' in sp: sp.remove('')

							#處理各測試結果資料(pre-short、open/short、testjet、analog、poweron、digital、boundary scan、analog powered、frequency、programming)
							if (len(sp)>0 and sp[0] == 'BLOCK') :
								db_sp.append(sp[1])
								db_sp.append(sp[2])


								# 若sp第一個元素為pwr_check、pwr_check_pro、pwr_check_iso則為power_on_result
								if(db_sp[0]=='pwr_check' or db_sp[0]=='pwr_check_pro' or db_sp[0]=='pwr_check_iso') : isPowerOn=True
								else : isPowerOn=False
								line = fp.readline()
								while line :		#block內有好幾個不同的測試
									line = line.strip('{@\n}')
									sp = line.split("|")
									print(line)
									if (len(sp)>0 and sp[0] == 'TJET'):
										db_sp_new = [machine,sn_code,sp[1],sp[3],EndTime,board]
										print (db_sp_new)

										# testjet fail RPT parsing
										if(db_sp_new[2]=='01'):

											line=fp.readline()
											while line:
										
												if (line == '}\n' or line.split("|")[0]=='{@DPIN'):break
												else:
													#移除頭尾@\n
													line = line.strip('{@\n}') 
													# split |
													sp = line.split("|")
													db_sp = []
													db_sp = sp[1].split()
													if(sp[0][:3]=='RPT' and len(db_sp)>0) :

														if(db_sp[0]=='Open'):
															#取得第二個元素並移除#字號後取得fail_no
															db_sp_new.append(db_sp[1].replace('#',''))
														elif(db_sp[0]=='Pin' and len(db_sp_new)<8):
															db_sp_new.append(db_sp[1])
															db_sp_new.append(db_sp[3])
														elif(db_sp[0]=='Measured'):
															db_sp_new.append(db_sp[1])
															db_sp_new.append(db_sp[3])									
															print(db_sp_new)
															del db_sp_new[6:11]
															
														elif('--' in db_sp[0]):break
														line=fp.readline()
													else :
														raise Exception("RPT format Error!")
											line = fp.readline()

									if (line == '}\n'): 
										if (db_sp[0] == 'pwr_check'):     #若讀到pwr_check結尾}代表接下來遇到的A-MEA皆屬於AnalogPowered測試結果
											isAnalogPowered=True     
											isPowerOn=False
										break
												
									else :
										line = line.strip('{@\n}') #移除頭尾@\n
										sp = line.split("{@")
										while '' in sp: sp.remove('')
										lines = []
										for item in sp:
											line = line.strip('{@\n}')
											lines += item.split("|")
										db_sp_new = db_sp + lines
										db_sp_new.insert(0,machine)
										db_sp_new.insert(1,sn_code)
										db_sp_new.append(EndTime)

										if (db_sp_new[4] == 'A-JUM') :
											del db_sp_new[3]		#刪除不必要元素
											del db_sp_new[3]
											db_sp_new.append(board)									
											print (db_sp_new)

										elif (db_sp_new[4] == 'TS'):
											del db_sp_new[3]		#刪除不必要元素
											del db_sp_new[3]
											db_sp_new.append(board)	
											print (db_sp_new)

										elif (db_sp_new[4] == 'A-CAP' or db_sp_new[4] == 'A-RES' or db_sp_new[4] == 'A-MEA' \
										 or db_sp_new[4] == 'A-DIO' or db_sp_new[4] == 'A-NFE' or db_sp_new[4] == 'A-PFE' \
										 or db_sp_new[4] == 'A-NPN' or db_sp_new[4] == 'A-PNP' or db_sp_new[4] == 'A-ZEN'):
											
											if (db_sp_new[7]=='LIM2'): db_sp_new.insert(8,None)
											if (db_sp_new[8]=='LIM2') : db_sp_new.insert(9,None) #沒有nominal,需補空值							
											if (len(db_sp_new)<13) : db_sp_new.insert(7,'') #沒有test_condition
											if(isPowerOn and db_sp_new[4] == 'A-MEA'):
												del db_sp_new[4] #analog powered不需要test_type
											elif(isAnalogPowered and db_sp_new[4] == 'A-MEA'):
												del db_sp_new[4] #PowerOn不需要component、test_type
											db_sp_new.append(board)	
											print (db_sp_new)
										line = fp.readline()
							

									
							elif (len(sp)>0 and sp[0] == 'PF'):
								line = line.strip('{@\n}') 
								sp = line.split("|") 
								db_sp_new=[machine,sn_code,EndTime,sp[2]]
								test_time=''
								if (sp[2]=='0'):
									# 若pass無BRC和fail時間
									db_sp_new.append(None)
									db_sp_new.append(None)
									db_sp_new.append(board)	
									print(db_sp_new)
								else:	
									line=fp.readline()						
									while line:	
										line = line.strip('{@\n}') 
										print(line)
										sp = line.split("|") 							
										db_sp=sp[1].split()
										if (sp[0]=='RPT' and db_sp[0]=='CHEK-POINT'):
											line=fp.readline()
											line = line.strip('{@\n}')  
											str_test_time=(line.split('|'))[1].replace('}','')
											test_time=dt.strptime(str_test_time,'%a %b %d %H:%M:%S %Y')
										elif (sp[0]=='RPT' and '(' in db_sp[0]):
											db_sp_new.append(test_time)
											db_sp_new.append(db_sp[0].replace('(','').replace(')',''))
											db_sp_new.append(board)	
											print(db_sp_new)
											# 處理多筆PF測試fail log
											del db_sp_new[4:7]
										elif ('End' in sp[1] or 'PIN' in sp[0]):
											break

										line=fp.readline()

							elif (len(sp)>0 and sp[0] == 'TS') :
								db_sp_new = [machine,sn_code,sp[1],sp[5],EndTime]
								db_sp_new.append(board)	
								print (db_sp_new)

								#若測試狀態為失敗,需parsing fail report
								if (sp[1]=='1'):
									line=fp.readline()
									test_time=''

									while line:

										if (line == '}\n'): break
							
										else:
											#移除頭尾@\n
											line = line.strip('{@\n}') 

											#split |
											sp = line.split("|")
											db_sp = [] 
											db_sp = sp[1].split()
																			
											#陣列第一個元素為RPT且第二個元素用空白切割後陣列長度大於0
											if (sp[0]=='RPT' and len(db_sp)>0):
												
												# 取得fail 時間
												if(db_sp[0]=='Shorts'):
													line=fp.readline().strip('{@\n}')
													str_test_time=(line.split('|'))[1].replace('}','')
													test_time=dt.strptime(str_test_time,'%a %b %d %H:%M:%S %Y')
											
												#陣列第一個元素為Short或Open
												elif (db_sp[0]=='Short' or db_sp[0]=='Open'):
													db_sp_new = [machine,sn_code,EndTime,test_time]
													#取得第fail_type為Short/Open
													db_sp_new.append(db_sp[0])
													#取得第二個元素並移除#字號後取得fail_no
													db_sp_new.append(db_sp[1].replace('#',''))
									 		
												#From為fail point起點 To為fail point終點
												elif(db_sp[0]=='From:' or db_sp[0]=='To:'):
													#若遇到針點為v需取下一行才是針點名稱
													if(db_sp[1]=='v'):
														line=fp.readline()
														point = (line.split())[0].split("|")
														db_sp_new.append(point[1])
													else :
														db_sp_new.append(db_sp[1])
													db_sp_new.append(db_sp[2])

													# ohms
													if(len(db_sp)>3 and db_sp[3]!='Open'):
														db_sp_new.append(db_sp[3])
													else:db_sp_new.append(None)
															
												#讀到Common視為其中一項fail結束
												elif (db_sp[0]=='Common'):
													db_sp_new.append(board)	
													print(db_sp_new)
													
												#fail Report結束
												elif ('End' in db_sp[0]):break

												line=fp.readline()

											else : line=fp.readline()	        
							
							# 上電測試-Digital result
							elif (len(sp)>0 and sp[0] == 'D-T') :
								db_sp_new=[]
								db_sp_new.insert(0,machine)
								db_sp_new.insert(1,sn_code)
								if(sp[1]=='1') : db_sp_new.append(sp[5])
								else : db_sp_new.append(sp[4])
								db_sp_new.append(sp[1])
								db_sp_new.append(EndTime)
								db_sp_new.append(board)						
								print (db_sp_new)
								
							# 上電測試-BoundaryScan result
							elif (len(sp)>0 and sp[0] == 'BS-CON') :
								db_sp_new=[]
								db_sp_new.insert(0,machine)
								db_sp_new.insert(1,sn_code)
								db_sp_new.append(sp[1])
								db_sp_new.append(sp[2])
								db_sp_new.append(EndTime)
								db_sp_new.append(board)	
								print (db_sp_new)


							line = fp.readline()
				else: line = fp.readline()
			fp.close()
			result["result"]='Success'
			mylog.logger.info(filename+'-'+str(result))
			return result
		except Exception as err:
			print(err)
			result["result"] = str(err)
			mylog.logger.error(filename+'-'+str(result))
			return result
	"""
	数据接口
	"""
	def __init__(self):
		self.parser = reqparse.RequestParser()
		self.parser.add_argument('file', required=True, type=FileStorage, location='files')

	def post(self):

		WriteDbResult = False
		isAnalogPowered=False
		isPowerOn=False

		machine = ''
		sn_code = ''
		EndTime = ''
		board = ''
		fixture_id=''
		program_id=''
		db_sp=[]
		db_sp_detail=[]
		insert_list=[]

		# get file
		file = request.files['file']
		filename=file.filename
		db_sp_detail.append(filename)
		mylog.logger.info(filename+'-Received')

		# 預設傳輸失敗
		result={}
		result["result"] = filename+" Fail"

		try:
			# check if file exist 
			if(filename==''):raise Exception("No file.")		

			# check duplicated
			if (self.CheckRepeat(filename)):raise Exception(filename+" is duplicated")

			# set file path
			path=os.path.abspath("..")+"/log/"

			# save file 
			file.save(os.path.join(path,filename))

			# check filesiz
			filesize=os.stat(path+filename).st_size
			if (filesize==0):raise Exception(filename+"file size should not be 0")

			# open file
			fp = open(path+filename, "r")
			line = fp.readline()

			# newlog parser
			while line :
				i=0
				line = line.strip('{\n}')
				sp=line.split(":")
				if (len(sp)==0 or ("=" in sp[0])):break
				else :
					i=i+1

					if (i==4 and sp[0]!='Cell_No'):db_sp_detail.append(1)
					if (sp[0].strip() == 'Board'):
						board=sp[1].split()[0].strip()
						db_sp_detail.append(board)
					elif (sp[0].strip() == 'Serial_Number'):
						sn_code=sp[1].strip()
						db_sp_detail.append(sn_code)
					elif (sp[0].strip() == 'ICT_STATION'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Cell_No'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Status'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Fail_code'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Start_time'):
						strtime=(sp[1]+sp[2]+sp[3]).strip().replace(' ','')
						start_time=dt.strptime(strtime.replace('-',''),'%Y%m%d%H%M%S')
						db_sp_detail.append(start_time)
					elif (sp[0].strip() == 'End_time'):
						strtime=(sp[1]+sp[2]+sp[3]).strip().replace(' ','')
						EndTime=dt.strptime(strtime.replace('-',''),'%Y%m%d%H%M%S')
						db_sp_detail.append(EndTime)
					elif (sp[0].strip() == 'Test_time'):db_sp_detail.append(sp[1].strip())				
					elif (sp[0].strip() == 'Operator'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Machine'):
						machine=sp[1].strip()
						db_sp_detail.append(machine)

					elif (sp[0].strip() == 'IP_ADDRESS'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Mac_Address'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Fixture_ID'):
						# db_sp.append(sp[1].strip())
						fixture_id_list=sp[1].strip().split("-")
						fixture_id=fixture_id_list[0]+'-'+fixture_id_list[1]+'-'+fixture_id_list[2]
						db_sp_detail.append(fixture_id)
					elif (sp[0].strip() == 'Program_ID'):
						program_id=sp[1].strip()
						db_sp_detail.append(program_id)
					elif (sp[0].strip() == 'Retest'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Average_vacuum'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'MIN_vacuum'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'MAX_vacuum'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Tester_TEMP'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'ESD_IMPEDANCE'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'ESD_VOLTAGE'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation1'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation2'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation3'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation4'):db_sp_detail.append(sp[1].strip())
					elif (sp[0].strip() == 'Deviation5'):
						db_sp_detail.append(sp[1].strip())

				line = fp.readline()

			line = fp.readline()

			# 如果db_sp_detail是空的，回傳檔案內容無法被剖析
			if(len(db_sp_detail)==0):raise Exception(filename+" can't be parsed")
			else:insert_list.append(self.CombineSqlStr(db_sp_detail,0))

			line=fp.readline()

			while line :
				if (line == '}\n' or line == '}}'): line = fp.readline()	#遇到'}'不處理
				else :
					line = line.strip('{@\n}') #去除外圍的括號與＠
					sp = line.split("|")
					db_sp = [] 
					
					if (len(sp)>0 and sp[0] == 'BLOCK') :

						db_sp.append(sp[1])
						db_sp.append(sp[2])
						# 若sp第一個元素包含(pwr_check)字串則該block區塊內的A-MEA為power_on_result
						if(db_sp[0]=='pwr_check' or db_sp[0]=='pwr_check_pro' or db_sp[0]=='pwr_check_iso') : isPowerOn=True
						else : isPowerOn=False 		
						line = fp.readline()

						while line :		#block內可能有好幾個不同的測試
							if(line[:8]=='{@BS-CON'):
								line = line.strip('{@\n}')  # 去除外圍的括號與＠
								sp = line.split("|")
								db_sp_new=[]
								db_sp_new.insert(0,machine)
								db_sp_new.insert(1,sn_code)
								db_sp_new.append(sp[1])
								db_sp_new.append(sp[2])
								db_sp_new.append(EndTime)
								db_sp_new.append(board)
								insert_list.append(self.CombineSqlStr(db_sp_new,7))
								break

							if(line[:5]=='{@D-T'):
								line = line.strip('{@\n}')  # 去除外圍的括號與＠
								sp = line.split("|")
								db_sp_new=[]
								db_sp_new.insert(0,machine)
								db_sp_new.insert(1,sn_code)
								db_sp_new.append(sp[5])
								db_sp_new.append(sp[1])
								db_sp_new.append(EndTime)
								db_sp_new.append(board)
								insert_list.append(self.CombineSqlStr(db_sp_new,6))
								line=fp.readline()
								if(line=='}\n'):
									line=fp.readline()

									if(line=='}\n'):break
									elif(line[:5]=='{@D-T'):
										line = line.strip('{@\n}')  # 去除外圍的括號與＠
										sp = line.split("|")
										db_sp_new=[]
										db_sp_new.insert(0,machine)
										db_sp_new.insert(1,sn_code)
										db_sp_new.append(sp[5])
										db_sp_new.append(sp[1])
										db_sp_new.append(EndTime)
										db_sp_new.append(board)
										insert_list.append(self.CombineSqlStr(db_sp_new,6))
										line=fp.readline()

							if (line[:6]=='{@TJET'):
								line = line.strip('{@\n}')  # 去除外圍的括號與＠
								sp = line.split("|")
								db_sp_new = [machine,sn_code,sp[1],sp[3],EndTime,board]
								
								insert_list.append(self.CombineSqlStr(db_sp_new,3))

								# testjet fail RPT parsing
								if(db_sp_new[2]=='01'):
									line=fp.readline()
									while line:
								
										if (line == '}\n' or line.split("|")[0]=='{@DPIN' or line.split("|")[0]=='{@INDICT'):break
										else:
											#移除頭尾@\n
											line = line.strip('{@\n}') 
											# split |
											sp = line.split("|")
											db_sp = []
											db_sp = sp[1].split()
											if(sp[0][:3]=='RPT' and len(db_sp)>0) :
												if(db_sp[0]=='Open'):
													#取得第二個元素並移除#字號後取得fail_no
													db_sp_new.append(db_sp[1].replace('#',''))
												elif(db_sp[0]=='Pin' and len(db_sp_new)<8):
													db_sp_new.append(db_sp[1])
													db_sp_new.append(db_sp[3])
												elif(db_sp[0]=='Measured'):
													db_sp_new.append(db_sp[1])
													db_sp_new.append(db_sp[3])
													insert_list.append(self.CombineSqlStr(db_sp_new,31))
													del db_sp_new[6:11]		
												elif('--' in db_sp[0]): break
												# else : raise Exception,"RPT format Error!"
												line=fp.readline()
											else :  raise Exception("RPT format Error!")
								break

							if (line == '}\n'): 
								if (db_sp[0] == 'pwr_check'):     #若讀到pwr_check結尾}則將接下來的A-MEA視為AnalogPoweredResult
									isAnalogPowered=True     
									isPowerOn=False
								break
								
							else :
								line = line.strip('{@\n}')
								sp = line.split("{@")

								while '' in sp: sp.remove('')
								lines = []
								for item in sp:
									# line = line.strip('{@\n}')
									lines += item.split("|")

								db_sp_new = db_sp + lines
								db_sp_new.insert(0,machine)
								db_sp_new.insert(1,sn_code)
								db_sp_new.append(EndTime)

								if (db_sp_new[4] == 'A-JUM'):
									del db_sp_new[3]		#刪除不必要元素
									del db_sp_new[3]
									db_sp_new.append(board)
									insert_list.append(self.CombineSqlStr(db_sp_new,1))
									
								elif (db_sp_new[4] == 'A-CAP' or db_sp_new[4] == 'A-RES' or db_sp_new[4] == 'A-MEA' \
									or db_sp_new[4] == 'A-DIO' or db_sp_new[4] == 'A-NFE' or db_sp_new[4] == 'A-PFE' \
						 			or db_sp_new[4] == 'A-NPN' or db_sp_new[4] == 'A-PNP' or db_sp_new[4] == 'A-ZEN'):
									
									dbType=4

									if (db_sp_new[7]=='LIM2'): db_sp_new.insert(8,None) #沒有nominal,需補空值	 
									if (db_sp_new[8]=='LIM2') : db_sp_new.insert(9,None) #沒有nominal,需補空值							
									if (len(db_sp_new)<13) : db_sp_new.insert(7,'') #沒有test_condition
									if(isPowerOn and db_sp_new[4] == 'A-MEA'):
										del db_sp_new[4]   #PowerOn不需要component、test_type
										dbType=5

									elif(isAnalogPowered and db_sp_new[4] == 'A-MEA'):
										del db_sp_new[4]     #analog powered不需要test_type
										dbType=8

									db_sp_new.append(board)
									insert_list.append(self.CombineSqlStr(db_sp_new,dbType))
									# analog_result需多寫入analog_result_3Month
									if (dbType==4):insert_list.append(self.CombineSqlStr(db_sp_new,41))
								line = fp.readline()

					# 模擬測試-Testjet
					elif (len(sp)>0 and sp[0] == 'TJET'):

						db_sp_new = [machine,sn_code,sp[1],sp[3],EndTime,board]
						
						insert_list.append(self.CombineSqlStr(db_sp_new,3))

						# testjet fail RPT parsing
						if(db_sp_new[2]=='01'):
							line=fp.readline()
							while line:
						
								if (line == '}\n' or line.split("|")[0]=='{@DPIN' or line.split("|")[0]=='{@INDICT'):break
								else:
									#移除頭尾@\n
									line = line.strip('{@\n}') 
									# split |
									sp = line.split("|")
									db_sp = []
									db_sp = sp[1].split()
									if(sp[0][:3]=='RPT' and len(db_sp)>0) :
										if(db_sp[0]=='Open'):
											#取得第二個元素並移除#字號後取得fail_no
											db_sp_new.append(db_sp[1].replace('#',''))
										elif(db_sp[0]=='Pin' and len(db_sp_new)<8):
											db_sp_new.append(db_sp[1])
											db_sp_new.append(db_sp[3])
										elif(db_sp[0]=='Measured'):
											db_sp_new.append(db_sp[1])
											db_sp_new.append(db_sp[3])
											insert_list.append(self.CombineSqlStr(db_sp_new,31))
											del db_sp_new[6:11]		
										elif('--' in db_sp[0]): break
										# else : raise Exception,"RPT format Error!"
										line=fp.readline()
									else :  raise Exception("RPT format Error!")
									

					elif (len(sp)>0 and sp[0] == 'PF'):
						line = line.strip('{@\n}') 
						sp = line.split("|") 
						db_sp_new=[machine,sn_code,EndTime,sp[2]]
						test_time=''
						if (sp[2]=='0'):
							# 若pass無BRC和fail時間
							db_sp_new.append(None)
							db_sp_new.append(None)
							db_sp_new.append(board)
							insert_list.append(self.CombineSqlStr(db_sp_new,9))
						else:	
							line=fp.readline()						
							while line:	
								line = line.strip('{@\n}') 
								sp = line.split("|") 
								db_sp=sp[1].split()
								if (sp[0]=='RPT' and db_sp[0]=='CHEK-POINT'):
									line=fp.readline()
									line = line.strip('{@\n}')  
									str_test_time=(line.split('|'))[1].replace('}','')
									test_time=dt.strptime(str_test_time,'%a %b %d %H:%M:%S %Y')
								elif (sp[0]=='RPT' and '(' in db_sp[0]):
									db_sp_new.append(test_time)
									db_sp_new.append(db_sp[0].replace('(','').replace(')',''))
									db_sp_new.append(board)
									insert_list.append(self.CombineSqlStr(db_sp_new,9))
									del db_sp_new[4:7]
								elif ('End' in sp[1] or 'PIN' in sp[0]):
									break

								line=fp.readline()	

					# 模擬測試-open_short
					elif (len(sp)>0 and sp[0] == 'TS') :
						db_sp_new = [machine,sn_code,sp[1],EndTime]
						db_sp_new.append(board)
						insert_list.append(self.CombineSqlStr(db_sp_new,2))
						#若測試狀態為失敗,需parsing fail report
						if (sp[1]=='1'):
							line=fp.readline()
							test_time=''
							while line:
								if (line == '}\n'): break #遇到'}'不處理
				
								else:
									#移除頭尾@\n split |
									line = line.strip('{@\n}') 
									sp = line.split("|") 									
									db_sp = [] 
									db_sp = sp[1].split()

									#陣列第一個元素為RPT且第二個元素用空白切割後陣列長度大於0
									if (sp[0]=='RPT' and len(db_sp)>0):

										# 取得fail 時間
										if(db_sp[0]=='Shorts'):
											
											line=fp.readline().strip('{@\n}')
											str_test_time=(line.split('|'))[1].replace('}','')
											test_time=dt.strptime(str_test_time,'%a %b %d %H:%M:%S %Y')
										
										#陣列第一個元素為Short或Open
										elif (db_sp[0]=='Short' or db_sp[0]=='Open'):
											db_sp_new = [machine,sn_code,EndTime]
											db_sp_new.append(test_time)
											#取得第fail_type為Short/Open
											db_sp_new.append(db_sp[0])
											#取得第二個元素並移除#字號後取得fail_no
											db_sp_new.append(db_sp[1].replace('#',''))

										#From為fail point起點 To為fail point終點
										elif(db_sp[0]=='From:' or db_sp[0]=='To:'):
											#若遇到針點為v需取下一行才是針點名稱
											if(db_sp[1]=="v"):
												line=fp.readline()
												point = (line.split())[0].split("|")
												
												db_sp_new.append(point[1])
											else :
												db_sp_new.append(db_sp[1])
											db_sp_new.append(db_sp[2])

											# ohms
											if(len(db_sp)>3 and db_sp[3]!='Open'):
												db_sp_new.append(db_sp[3])
											else:db_sp_new.append(None)
													
										#讀到Common視為其中一項fail結束
										elif (db_sp[0]=='Common'):
											db_sp_new.append(board)
											insert_list.append(self.CombineSqlStr(db_sp_new,21))
											
										#fail Report結束
										elif ('End' in db_sp[0]): break

										line=fp.readline()

									else : line=fp.readline()	        
					
					# 上電測試-Digital result
					elif (len(sp)>0 and sp[0] == 'D-T') :
						db_sp_new=[]
						db_sp_new.insert(0,machine)
						db_sp_new.insert(1,sn_code)
						db_sp_new.append(sp[5])
						db_sp_new.append(sp[1])
						db_sp_new.append(EndTime)
						db_sp_new.append(board)
						insert_list.append(self.CombineSqlStr(db_sp_new,6))
					
					# 上電測試-BoundaryScan result
					elif (len(sp)>0 and sp[0] == 'BS-CON') :
						db_sp_new=[]
						db_sp_new.insert(0,machine)
						db_sp_new.insert(1,sn_code)
						db_sp_new.append(sp[1])
						db_sp_new.append(sp[2])
						db_sp_new.append(EndTime)
						db_sp_new.append(board)
						insert_list.append(self.CombineSqlStr(db_sp_new,7))

					line = fp.readline()

			fp.close()

			WriteDbResult=self.WriteToDb(insert_list)

		except Exception as err:
			result['result'] = str(err)
			# print("[error]: {0}".format(str(err))
			mylog.logger.error(filename+'-'+str(err))
		
		if WriteDbResult :
			#移動處理完的檔案到上一層的processedlog資料夾中
			shutil.move(path+filename,os.path.abspath("..")+"/log_done/")
			result['result'] = 'Success'
			mylog.logger.info(filename+'-'+result['result'])
		return result

	def CheckRepeat(self,filename):
		conn = mysql2.connect()
		cursor = conn.cursor()
		sql='''SELECT count(*) as count from ICT_Project.ict_detail_result where filename='{}' '''.format(filename)
		cursor.execute(sql)
		result=cursor.fetchall()
		# print(result)
		# 若有取得相同檔名回傳true表示log重複
		return (result[0]['count']>0)

	def CombineSqlStr(self,lists,type):
		#type 0:  ict_detail_result
		#type 1:  jumper測試
		#type 2:  short測試
		#type 21: short fail RPT
		#type 3:  testjet
		#type 31: testjet fail RPT
		#type 4:  analog
		#type 41: analog_result_3Month
		#type 5:  power on 
		#type 6:  digital
		#type 7:  boundary scan
		#type 8:  analog_powered
		#type 9:  PF test
		if (type == 0) :
			# Items = 'insert ignore into ict_result(board,operator,machine,sn,status,start_time,end_time,log_time) values ('
			sql = 'insert ignore into ICT_Project.ict_detail_result(filename,board,sn,ict_station,cell_no,status,fail_code,start_time,end_time,total_time,operator,machine,machine_ip,machine_mac,fixture_id,program_id,retest,avg_vacuum,min_vacuum,max_vacuum,tester_temp,esd_impedance,esd_voltage,deviation1,deviation2,deviation3,deviation4,deviation5) values ('
		elif (type == 1) :
			sql = 'insert ignore into ICT_Project.preshort_result(machine,sn,component,status,measured,limit_type,high_limit,low_limit,end_time,board) values ('
		elif (type == 2) :
			sql = 'insert ignore into ICT_Project.open_short_result(machine,sn,status,end_time,board) values ('
		elif (type == 21) :
			sql = 'insert ignore into ICT_Project.open_short_fail(machine,sn,end_time,fail_time,fail_type,fail_no,from_point,from_BRC,from_ohms,end_point,end_BRC,end_ohms,board) values ('
		elif (type == 3) :
			sql = 'insert ignore into ICT_Project.testjet_result(machine,sn,status,device,end_time,board) values ('
		elif (type == 31) :
			sql = 'insert ignore into ICT_Project.testjet_fail(machine,sn,status,device,end_time,board,fail_no,pins,node,measured,BRC) values ('		
		elif (type == 4) :
			sql = 'insert ignore into ICT_Project.analog_result(machine,sn,component,block_status,test_type,status,measured,test_condition,limit_type,nominal,high_limit,low_limit,end_time,board) values ('
		elif (type == 41) :
			sql = 'insert ignore into ICT_Project.analog_result_3Month(machine,sn,component,block_status,test_type,status,measured,test_condition,limit_type,nominal,high_limit,low_limit,end_time,board) values ('
		elif (type == 5) :
			sql = 'insert ignore into ICT_Project.power_on_result(machine,sn,power_check_type,block_status,status,measured,power_check,limit_type,nominal,high_limit,low_limit,end_time,board) values ('				
		elif (type == 6) :
			sql = 'insert ignore into ICT_Project.digital_result(machine,sn,component,status,end_time,board) values ('
		elif (type == 7) :
			sql = 'insert ignore into ICT_Project.boundary_scan_result(machine,sn,component,status,end_time,board) values ('
		elif (type == 8) :
			sql = 'insert ignore into ICT_Project.analog_powered_result(machine,sn,component,block_status,status,measured,test_condition,limit_type,nominal,high_limit,low_limit,end_time,board) values ('
		elif (type == 9) :
			sql = 'insert ignore into ICT_Project.pins_check_result(machine,sn,end_time,status,fail_time,BRC,board) values ('


		for item in lists:
			if str(item)=="None":sql=sql+'null'+','
			else : sql = sql + '"' + str(item) + '"' + ','
		sql = sql.strip(',')
		sql += ')'
		return(sql)

	def WriteToDb(self,insert_list):
		try :
			# print(insert_list)
			conn = mysql2.connect()
			cursor = conn.cursor()

			for item in insert_list:
				cursor.execute(item)

			conn.commit()
			cursor.close()
			conn.close()
			return True	
		except Exception as inst:
			print('ICT Data MySql Write Err:'+item)
			print(str(inst))

			mylog.logger.error(item+'-'+str(inst))
			try:
				conn.rollback()
			except: pass
		return False
		
api.add_resource(LogParser, '/logparser/')


if __name__ == '__main__':
	app.run(debug=True)