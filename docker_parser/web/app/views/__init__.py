#!/usr/bin/python
# -*- coding:utf-8 -*-
from flask import Blueprint
from flask_cors import CORS

views_blueprint = Blueprint('views', __name__)

CORS(views_blueprint)

from . import exp_data
from . import FixedNode
from . import fixture
from . import fulllog
from . import fulllog_batch
from . import logparser
from . import newlog
from . import testjet_limit
from . import wirelist

